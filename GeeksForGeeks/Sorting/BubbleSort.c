#include<stdio.h>



void printArr(int A[],int n){
int i=0;
	for(i=0;i<n;i++)
	{
	printf(" %d", A[i]);
	}

}
void BubbleSort(int A[],int n)
{
	int pass;
	int i;
	int temp;
	int m=0;
	for( pass=n;pass >= 0;pass--)
	{
		for( i=0;i<pass-1;i++)
		{
			if(A[i] > A[i+1])
			{
			   temp=A[i]	;
			  A[i]=A[i+1];
			  A[i+1]=temp;
			  		
			}
		m++;
		printf("%d:",m);
		printArr(A,10);
		printf("\n");			
		}

	}

}

void BubbleSortEfficient(int A[],int n)
{
   int pass;
   int i;
   int temp;
   int swapped=1;
   int m=0;
   for(pass=n;pass>=0 && swapped ;pass--)
    {
	swapped=0;
	for(i=0;i<pass-1;i++)
	{
		if(A[i]>A[i+1])
		{
			temp=A[i];
			A[i]=A[i+1];
			A[i+1]=temp;
		m++;
		printf("%d :",m);
		printArr(A,10);
		printf("\n");	
		swapped=1;
		}
	}
	
    }
		

}

main()
{
int A[10]={3,1,2,3,4,1,5,8,9,0};
printArr(A,10);
printf("\n");
BubbleSort(A,10);
printArr(A,10);
printf("\n----------------Bubble Sort Efficiently ---------------------\n");
printf("\n");
int  B [10]={3,1,2,3,4,1,5,8,9,0};
BubbleSortEfficient(B,10);
printArr(B,10);
return 0;
}
