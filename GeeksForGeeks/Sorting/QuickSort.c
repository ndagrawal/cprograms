/* The following problem is implementation of quicksort using recursion n	
	
	 
	

*/

#include<stdio.h>
void printArr(int arr[],int n){
int count =0;
	while(count<n)
	{
	printf("%d ",arr[count]);
	count++;
	}

}
int partition(int arr[],int low, int high) 
{
	int a,down,up,temp;

	a=arr[low];	/*a is the element whose final position is sought */
	
	down=low;
	up=high;
	
	while(down<up){
		while(arr[down]<= a && down < high )
			down++; 		/*move up the array */
		while(arr[up]>a)
			up--;			/* move down the array */
		
		if(down<up){
		/* 	interchange arr[down] and arr[up]	*/
	
		temp=arr[down];
		arr[down]=arr[up];
		arr[up]=temp;
		}
	
	}
	
	arr[low]=arr[up];
	arr[up]=a;
	
return up;

}

void QuickSort(int arr[],int low,int high)
{
int pivot;

	/*Termination condition */

	if(high > low){
	pivot=partition(arr,low,high);
	printf("pivot :%d\n",pivot); 
	printf("\n :");
	printArr(arr,10);
	QuickSort(arr,low,pivot-1);
	QuickSort(arr,pivot+1,high);
	}	

}




main()
{
int arr[10]={3,2,1,4,1,3,4,7,4,2};
QuickSort(arr,0,9);
printArr(arr,10);


return 0;
}





