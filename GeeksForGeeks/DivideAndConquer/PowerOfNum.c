/* the following program helps to implement the power of the number 

pow(x,n)
Here we are considering the floating value for the x and negative value of the power... 
x- base 
n- power.

*/

#include<stdio.h>
#include<limits.h>
#include<math.h>

float power(float x, int  y){
float temp;	
	if(y==0)
	   return 1;
	
	 temp=power(x,y/2);
	
  	if(y%2==0){
	return temp*temp;
	}	
	else{
	
	if(y>0)
	return x*temp*temp;
	else
	return temp*temp/x;
	}
}

main(){
printf(" 3.0 ^ -2 %f",power(3.0,-2));
return 0;
}
