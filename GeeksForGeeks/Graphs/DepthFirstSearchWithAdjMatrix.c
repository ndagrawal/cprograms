#include<stdio.h>
#include<stdlib.h>

struct Graph{
int V; // To represent number the vertex...
int E; //To represent number the Edge.....
int **Adj; // Two dimensional matrix to form the adjacency matrix... 
};



int visited[5];
void DFS(struct Graph *G, int v){
int i;
	visited[v]=1;
	printf("%d ",v);
	for(i=0;i< G->V;i++){
	if(!visited[i] && G->Adj[v][i]){
	
		DFS(G,i);
	}
	}



}
void DFSTraversal(struct Graph *G){

	int i=0;
	
	for(i=0;i< G->V;i++){
	visited[i]=0;
	}

	for(i=0;i<G->V;i++)
		DFS(G,i);

}





struct Graph *adjMatrixOfGraph(){
	
	int i;    //for scanning the edges between them .... 
	int u,v; // for loop while initliasing the  adjacency matrix... 
	struct Graph *G=(struct Graph*) malloc(sizeof(struct Graph)); //

	if(!G){
	printf("Memory Error");
	return;
	}

	printf("Number of Vertices");		
	scanf("%d",&G->V);
	printf("Number of Edges");
	scanf("%d",&G->E);

	G->Adj =malloc(sizeof(int*) * G->V);
	for (i = 0; i < G->V; i++)
    		G->Adj[i] =malloc(sizeof(int)* G->V);

	if(!G->Adj)
	{
	printf("Memory Error");
	return;
	}

	
	for(u=0;  u < G->V; u++){
	for(v=0; v < G->V; v++){
	 G->Adj[u][v]=0;  //initalising the complete adjacency matrix to zero.
	}
	}
	
	//Enter the edges.. and the vertices.
	//We are considering this graph as undirected one ... 
	for(i=0;i< G->E;i++)
	{
	printf("Reading Edges");
	scanf(" %d %d",&u,&v);
	if(u > G->E || v > G->E){
	printf("Input Bigger Than Size of vertice/ edge ");
	return;
	}
	G->Adj[u][v]=1;
	G->Adj[v][u]=1;
	
//if the graph is directed then you should only update u to v and not v to u. 

	
	}

	
	for(u=0;  u < G->V; u++){
	for(v=0; v < G->V; v++){
	printf("%d", G->Adj[u][v]);  //initalising the complete adjacency matrix to zero.
	}
	printf("\n");
	}

	
return G;
}

main()
{
struct Graph *G1=adjMatrixOfGraph();
DFSTraversal(G1);

//struct Graph *adjMatrixOfGraph(){
printf("Successful");
return 0;
}
