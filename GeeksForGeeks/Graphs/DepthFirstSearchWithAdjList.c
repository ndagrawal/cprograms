#include<stdio.h>
#include<stdlib.h>
#include"AdjList.h"

void DFSTraversal(struct Graph *G,int m,int visited[]){
	visited[m]=1;
	struct AdjListNode *aa;
	printf("%d",m);
	aa=G->array[m].head;
	while(aa)
	{
		if(!visited[aa->data]){
		DFSTraversal(G,aa->data,visited);
		}
	aa=aa->next;					
	}
}

void DFS(struct Graph *G,int visited[]){
	
	int i;
	for(i=0;i< G->V;i++)
	{	
	 visited[i]=0;
	}
	for(i=0;i<G->V;i++)
	     {
		if(!visited[i])
		DFSTraversal(G,i,visited);
	      }		
	}

main(){
	struct Graph *G = CreateGraph(8);
	int visited[8];
	addEdge(G,0,1);
	addEdge(G,1,4);	
	addEdge(G,1,2);
	addEdge(G,2,3);
	addEdge(G,2,5);
	addEdge(G,4,5);
	addEdge(G,5,6);
	addEdge(G,5,7);
	DFS(G,visited);
return 0;
}
