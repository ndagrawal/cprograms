#include<stdio.h>
#include<stdlib.h>
struct AdjListNode {
int data;
struct AdjListNode *next;
};

struct AdjList{
	struct AdjListNode *head;
};

struct Graph{
int V;
struct AdjList *array;
};


	

struct Graph* CreateGraph(int V)
{
    struct Graph* graph = (struct Graph*) malloc(sizeof(struct Graph));
    graph->V = V;
 
    // Create an array of adjacency lists.  Size of array will be V
    graph->array = (struct AdjList*) malloc(V * sizeof(struct AdjList));
 
     // Initialize each adjacency list as empty by making head as NULL
    int i;
    for (i = 0; i < V; ++i)
        graph->array[i].head = NULL;
 
    return graph;
}
 

struct AdjListNode *newNode(int dest){

	struct AdjListNode *S=(struct AdjListNode *) malloc(sizeof(struct AdjListNode));
	S->data=dest;
	S->next=NULL;
	
return S;
}

void addEdge(struct Graph *G,int src,int dest){

	struct AdjListNode *list=newNode(dest);
	if(G->array[src].head == NULL){
	G->array[src].head = list;
	}
	else{
	list->next=G->array[src].head;
	G->array[src].head = list;
	}

	//for undirected graph now also connect the dest to source... 

	struct AdjListNode *list1=newNode(src);
	if(G->array[dest].head==NULL){
	G->array[dest].head=list1;
	}
	else{
	list1->next=G->array[dest].head;
	G->array[dest].head = list1;
	}
	
	
}

void printGraph(struct Graph *G,int V){

int i;
	struct AdjListNode *current ;
	for(i=0;i<G->V;i++){
		current=G->array[i].head;
		while(current){
		printf(" %d",current->data);
		current = current->next;
		}

		printf("\n");	
	}

}

main(){

int V;
	V=5;
	struct Graph *G=CreateGraph(5);

	addEdge(G,0,1);
	addEdge(G,0,2);
	addEdge(G,1,2);
	addEdge(G,2,3);
	addEdge(G,3,4);
	addEdge(G,4,0);
	printGraph(G,V);

return 0;
}
