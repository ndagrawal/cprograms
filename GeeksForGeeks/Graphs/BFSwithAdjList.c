#include<stdio.h>
#include"AdjList.h"
#include"BasicQueue.h"
#include<stdlib.h>

void BFSUtil(struct Graph *G,int visited[],int startNode){
	
	
struct QueueList *Q = createQueue(8);
	Enqueue(Q,startNode);
	int node=0;
	while(!IsEmptyQueue(Q)){
	node = Dequeue(Q);
	//printf("%d",node);
	if(!visited[node])
	{
		visited[node] = 1;
		printf("%d",node);
	}

	struct AdjListNode *current=G->array[node].head;
	while(current){

		if(visited[current->data]!=1){
			Enqueue(Q,current->data);
		}

	current=current->next;
	}
	}
	
	

}

void BFS(struct Graph *G,int visited[]){

int i;
	for(i=0;i<G->V;i++)
	visited[i]=0;
	
	//if(!visited[0])
	BFSUtil(G,visited,0);


}
main(){
struct Graph *G=CreateGraph(8);

	int visited[8];
	addEdge(G,0,1);
	addEdge(G,1,2);
	addEdge(G,2,3);
	addEdge(G,2,5);
	addEdge(G,5,6);
	addEdge(G,4,5);
	addEdge(G,1,4);
	addEdge(G,5,7);

	BFS(G,visited);

return 0;
}

