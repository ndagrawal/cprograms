#include<stdio.h>
#include<stdlib.h>
#define NO_OF_CHARS 256 
void RemoveDuplicates(char *str){
int *hashtable;
int i;
hashtable=(int *)malloc(sizeof(int)*NO_OF_CHARS);
int ip_add=0;		/* index of the present string	*/
int res_add=0;		/*	index of the resultant string */
char temp;		/*using the temp variable to swap those variables */

	

	for(i=0;i<NO_OF_CHARS;i++)
	{
		hashtable[i]=0;
	}

	while(*(str + ip_add))
	{
		temp=*(str + ip_add);
		if(hashtable[temp]==0)
		{
			hashtable[temp]=1;
			
			*(str + res_add) = *(str  + ip_add);
			res_add++; 
		}
		
		ip_add++;
	}

*(str+res_add)='\0';
free(hashtable);
}

void printAllDuplicates(char *str){

int *count = (int *) malloc (sizeof(int) * NO_OF_CHARS);
	/*initliasing the array to zero so that no zero values remain */
int ip_add; /*current index of the string */
int i;
	for(i=0;i<NO_OF_CHARS;i++)
	{
		count[i]=0;
	}
	
	ip_add=0;	
	while(*(str + ip_add))
	{
		count[ *(str+ip_add) ]++;
		ip_add++;
	}

	for(i=0;i<NO_OF_CHARS;i++)
	{
		if(count[i] > 1)
		{
			printf("character : %c and count : %d ",i,count[i]);	
		}
	}
}


main()
{
char str[]="geeksforgeeks";
printAllDuplicates(str);
RemoveDuplicates(str);
printf("%s",str);
return 0;
}
