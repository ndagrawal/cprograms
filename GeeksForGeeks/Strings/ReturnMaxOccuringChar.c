#include<stdio.h>
#include<stdlib.h>

#define NO_OF_CHARS 13

int *getMaxArray(char s[]){
int *count;
count = (int *)malloc(NO_OF_CHARS * sizeof(int));
int i;

	for(i=0;*(s+i);i++)
	{
		count[*(s+i)]++;
	}

return count;	

}

/*--------------------*/

int getIndex(int *A){
int max;
int i=0;
for(i=0;i<NO_OF_CHARS-1;i++)
{
	if(A[i] > A[i+1])
	{
	max=A[i];
	}
	else
	{
	max=A[i+1];
	}
}
return max;
}

int getMaxOccuringIndex(char *s){
int maxoccur;
int *count =getMaxArray(s);
maxoccur = getIndex(count);
return maxoccur;
}

/*--------------------- */

main()
{

char sample[]="Sample String";
int index;
index=getMaxOccuringIndex(sample);
printf("%c",sample[index]);
return 0;
}
