#include<stdio.h>
#include<stdlib.h>
#include"BuildMaxHeap.h"


void HeapSort(int A[], int n){

struct PriorityQueue *h=CreateHeap(n,0);
int old_count;
int i;
int temp;
	if(!h)
	{
	   printf("Memory Error ");
	   return;
	}

	BuildHeap(h,A,n);
	old_count = h->count;

	

	for(i=n-1;i>=0;i--)
	{
		temp=h->array[0];
		h->array[0]=h->array[h->count - 1 ];
		A[i] = temp;
		h->count--;
		PercolateDown(h,0);
	}
	
	h->count = old_count;


} 


main(){
int A[11]={23,41,21,45,65,100,1000,43,64,98,99};
HeapSort(A,11);
int n = sizeof(A)/sizeof(int);
int i;

printf("\n Sorted Array -----  \n");
	for( i=0;i< n ;i++)
	{
	printf(" %d ",A[i]);
	}

return 0;
}
