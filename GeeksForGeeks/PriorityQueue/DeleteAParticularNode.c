#include<stdio.h>
#include<stdlib.h>
#include"BuildMaxHeap.h"



//here we are passing the array index.... 

void deleteParticularNode(struct PriorityQueue *h,int i){


	if(i>h->count)
	{
	printf("Memory Error");
	return;
	}
	
	h->array[i]=h->array[h->count - 1];
	PercolateDown(h,i);
	h->count--;
}


main(){
int A[]={100,21,30,43,11,0,12,13,19,101};
int n = sizeof(A)/sizeof(int);
struct PriorityQueue *h = CreateHeap(n,0);
BuildHeap(h,A,n);
deleteParticularNode(h,3);
int i;
printf("\n Printing the heap after deletion");
for(i=0;i<h->count; i++)
{
	printf(" %d",h->array[i]);

}



return 0;
}

