#include<stdio.h>
#include<stdlib.h>
//for this program should be ..... heap_type = 0;

struct PriorityQueue{
int *array;	  //
int count;	 //tells us how many elements in the priority queue.
int capacity;	// tells the size of the priority Queue..
int heap_type; // tells you what kind of heap it is ... (min or max heap).
};

struct PriorityQueue *CreateHeap(int capacity,int heap_type){
struct PriorityQueue *h = (struct PriorityQueue *)malloc(sizeof(struct PriorityQueue));

	if(!h)
	{
	printf("Memory Error");
	return NULL;
	}
	
	h->capacity = capacity;
	h->count=0;
	h->heap_type=heap_type; /*	minheap =0;
				maxheap =1;
			*/

	h->array=(int *)malloc(sizeof(int) * h->capacity);
	if(!h->array){
	printf("Memory Error");
	return NULL;
	}

return h;	
}

void ResizeHeap(struct PriorityQueue *h){

	int *array_old=h->array;
	
	h->array=(int *)malloc(sizeof(struct PriorityQueue) * h->capacity * 2);
	
	if(!h->array){
	printf("Memory Error ");
	return;
	}

	int i=0;
	for(i=0;i < h->capacity; i++)
	{
		h->array[i] = array_old[i];
	}
	
	h->capacity*=2;
	free(array_old);

}

//finding the left and right child ...
int LeftChild(struct PriorityQueue *h,int i){

int left = 2 * i+1;
	//printf("%d ",left); 
	if(left >= h->count)
	return -1;

return left;
}

int RightChild(struct PriorityQueue *h,int i){

int right = 2 * i + 2;

	if(right >= h->count){
	return -1;
	}
	
return right;
}

//code related to the max heap....

void PercolateDown(struct PriorityQueue *h,int i){

	int temp,min,left,right;
	left   = LeftChild(h,i); 
	right  = RightChild(h,i);
	
	if(left != -1 && h->array[left] <  h->array[i])
	min = left;
	else
	min = i;

	if(right !=-1 && h->array[right] <h->array[min]){
	min=right;
	}


	if(min !=i)
	{
		temp= h->array[i];
		h->array[i]=h->array[min];
		h->array[min]=temp;
		
		PercolateDown(h,min);
	} 

}

void BuildHeap(struct PriorityQueue *h,int A[],int n){
	
	int i;

	
	while(n > h->capacity)
	{
	ResizeHeap(h);
	}
	
	for(i=0;i<n;i++){
	h->array[i]=A[i];
	}

	h->count=n;
	
	for(i= (n-1)/2 ; i>=0; i--){
	//printf("%d ",i);
	PercolateDown(h,i);
	}
	
	i=0;
	for(i=0;i<n;i++)
	printf("%d ",h->array[i]);

}

int getMaximum(struct PriorityQueue *h){
int max;

	if(h->count == 0)
	return -1;

	max = h->array[0];

return max;
}

int DeleteMin(struct PriorityQueue *h){

	int data;
	if(h->count ==0){
		return -1;
	}
	
	data = h->array[0];
	h->array[0]=h->array[h->count - 1 ];
	h->count--;
	PercolateDown(h,0);
	return data;
}

void insert(struct PriorityQueue *h,int data){
int i;

	
	if(h->count == h->capacity)
		ResizeHeap(h);
	h->count++;
	i=h->count - 1;
	while(i>0 && data <  h->array[(i-1)/2])
	{
	printf("%d",i);
	h->array[i]=h->array[(i-1)/2];
  	i = (i-1)/2;
	printf("%d ",i);
	}
	//printf("fef");
	h->array[i]=data;
}



