// implementing the queues using arrays .... 

#include<stdio.h>
#include<stdlib.h>

struct QueueList{
int front,rear;	//index of the first and last element.... 
int capacity;	//size of the queue list .... 
int *array;	// size of the queue... 
};

struct QueueList *createQueue(int size){

struct QueueList *Q = (struct QueueList *)malloc(sizeof(struct QueueList));
	if(!Q)
	{
	printf("Memory Error");
	return NULL;
	}
	Q->capacity=size;
	Q->front=-1;
	Q->rear=-1;
	Q->array=(int *)malloc(sizeof(int) * Q->capacity);
	
	if(!Q->array)
	{
	printf("Memory Error");
	return NULL;
	}
return Q;
}

void Enqueue(struct QueueList *Q,int data){
	
	if(!Q)
	{
	 return;
	}
	
	if(IsFullQueue(Q)){
	return;
	}
	else{
	Q->rear = (Q->rear +1) % (Q->capacity);
	Q->array[Q->rear]=data;	
	if(Q->front==-1)
	{
	Q->front = Q->rear;
	}
	
	}
}


int Dequeue(struct QueueList *Q){
	
int data;

	if(IsEmptyQueue(Q)){
	return;
	}
	else{
	data = Q->array[Q->front];
	if(Q->front == Q->rear){
		Q->front = Q->rear = -1;
		
	}
	else{
		Q->front = (Q->front+1)	% (Q->capacity);
	
	}

	}
return data;
}


int IsEmptyQueue(struct QueueList *Q){
	
	if(Q->front== -1){
	return 1;
	}
return 0;
}

int IsFullQueue(struct QueueList *Q){

	if(Q->rear== Q->capacity-1)
	{
		return 1;	
	}
	
return 0;
}

void DeleteQueue(struct QueueList *Q){

	if(Q)
	{
		if(Q->array)
		free(Q->array);
	
	free(Q);
	}

}


main(){


struct QueueList *Q=createQueue(8);

	Enqueue(Q,1);
	
	Enqueue(Q,2);

	Enqueue(Q,3);

	Enqueue(Q,4);

	Enqueue(Q,5);

	printf("Element : %d ",Dequeue(Q));
	
//	printf("Element in the front %d",FrontOfQueue(Q));
	return 0;
}
