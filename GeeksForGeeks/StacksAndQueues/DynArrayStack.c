//The Following program implements the stack using arrays.. 


#include<stdio.h>
//As we are going to use malloc function .. so .
#include<stdlib.h>

//Structure to define arraystack.
//It grows up in size once full.


struct ArrayStack{
int top;
int capacity;
int *array;
};

//Function Declarations.

struct ArrayStack *CreateStack();
int pop(struct ArrayStack *s);
void push(struct ArrayStack *s,int data);
int IsFullStack(struct ArrayStack *s);
int IsEmptyStack(struct ArrayStack *s);
void DoubleStack(struct ArrayStack *s);
void deleteStack(struct ArrayStack *s);


main()
{

//Enter the size of the stack ... 
int size;

//Simple iterator.
int i;

//Variable to take the name ..
int elements;

//printf("Enter the size of the element");
//scanf("%d",&size);

//Create allocate a space for the stack.
struct ArrayStack *s=CreateStack();

	
	printf("Enter the number");	
	scanf("%d",&elements);
	push(s,elements);
	

printf("\n popped out element %d",pop(s));

return 0;
}


void deleteStack(struct ArrayStack *s){

	if(s)
	{	
		if(s->array)
		free(s);
	free(s);
	}

}
struct ArrayStack *CreateStack(int size){

	//Allocate a space for the array. 
struct ArrayStack *s=malloc(sizeof(struct ArrayStack));

	//Check whether the memory is assinged or not ... 

	if(!s)
	{
  	printf("Fail to assign memory ");
 	   return NULL;
	}

	s->capacity=1; //Assign a size to the capacity.
	s->top = -1;	//To mark it as empty array.	
	s->array= malloc(sizeof(s->capacity *sizeof(int)));

	if(s->array==NULL)
	{
		return NULL;
	}
return s;

}

int IsEmptyStack(struct ArrayStack *s)
{
	if(s->top==-1)
	{
	printf("Empty Stack");
	return 1;
	}
	else 
	return 0;
}

int IsFullStack(struct ArrayStack *s)
{

 	if(s->top==(s->capacity-1)){
	
	printf("StackFull");
	return 0;
	}
	else
	{
	return 1;
	}
				
}

	
int pop(struct ArrayStack *s){

	int element;

	if(IsEmptyStack(s)){
	return 0;
	}
	else {
	element= s->array[s->top];	
	s->top--;
	return element;	
	}
	
}


void DoubleStack(struct ArrayStack *s)
{
	s->capacity = s->capacity * 2;
	s->array=(int *) realloc(s->array,s->capacity * sizeof(int));
	
}

void push(struct ArrayStack *s,int num)
{
	if(IsFullStack(s))
	{
	printf("Stack OverFlow");
	DoubleStack(s);
	}
	 s->array[++s->top]=num;	

	
}
