//The Following program implements the stack using arrays.. 

#include<stdio.h>
//As we are going to use malloc function .. so .
#include<stdlib.h>

//Structure to define arraystack.
struct ArrayStack{
int top;
int capacity;
int *array;
};

//Function Declarations.
struct ArrayStack *CreateStack(int size);
int pop(struct ArrayStack *s);
void push(struct ArrayStack *s,int data);
int IsFullStack(struct ArrayStack *s);
int IsEmptyStack(struct ArrayStack *s);
int top(struct ArrayStack *s);
void deleteStack(struct ArrayStack *s);

main()
{

//Enter the size of the stack ... 
int size;

//Simple iterator.
int i;

//Variable to take the name ..
int elements;

printf("Enter the size of the element");
scanf("%d",&size);

//Create allocate a space for the stack.
struct ArrayStack *s=CreateStack(size);
	
	printf("Enter the numbers");
	for(i=0;i<size;i++)
	{	
	scanf("%d",&elements);
	push(s,elements);
	}

printf("\n popped out element %d",pop(s));
printf("\n Now the top element is %d",top(s));

deleteStack(s);

printf("\n Now the top element is %d",top(s));
printf("\n deleted Stack ... ");


	
return 0;
}
void deleteStack(struct ArrayStack *s){
	
	if(s)
	{
		if(s->array)
		{
		free(s->array);
		}
	free(s);
	}

}
struct ArrayStack *CreateStack(int size){

	//Allocate a space for the array. 
struct ArrayStack *s=malloc(sizeof(struct ArrayStack));

	//Check whether the memory is assinged or not ... 

	if(!s)
	{
  	printf("Fail to assign memory ");
 	   return NULL;
	}

	s->capacity=size; //Assign a size to the capacity.
	s->top = -1;	//To mark it as empty array.	
	s->array= malloc(sizeof(s->capacity *sizeof(int)));

	if(s->array==NULL)
	{
		return NULL;
	}
return s;

}

int IsEmptyStack(struct ArrayStack *s)
{
	if(!s)
	{
	printf("Stack does not exists ");
	return -1;
	}
	if(s->top==-1)
	{
	printf("Empty Stack");
	return 1;
	}
	else 
	return 0;
}

int IsFullStack(struct ArrayStack *s)
{
	
	if(!s)
	{
	printf("Stack does not exists ");
	return -1;
	}

	



 	if(s->top==(s->capacity-1)){
	printf("Stack Full");
	return 1;	
	}	
	else
	{
	return 0;
	}
}

	
int pop(struct ArrayStack *s){

	int element;

	
	if(!s)
	{
	printf("Stack does not exists ");
	return -1;
	}


	if(IsEmptyStack(s)){
	return 0;
	}
	else {
	element= s->array[s->top];	
	s->top--;
	return element;	
	}
	
}

int top(struct ArrayStack *s){
	int element=-1;

	
	if(!s)
	{
	printf("Stack does not exists ");
	return;
	}
	
	if(IsEmptyStack(s)){
	return 0;
	}
	else
	{
	element=s->array[s->top];
	}
return element;
}

void push(struct ArrayStack *s,int num)
{
	
	if(!s)
	{
	printf("Stack does not exists ");
	return;
	}
	
	if(IsFullStack(s))
	{
	printf("Stack OverFlow");
	}else{	
	 s->array[++s->top]=num;	

	}
}
