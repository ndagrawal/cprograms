/*this program is the implementation of the stack using the linked List data Structure....  */ 
#include<stdio.h>
#include<stdlib.h>

/*In this program we dont need to check that the stack is full or not as we are implementing the stack using dynamic data structure */

struct ListNode {

	int data;
	struct ListNode *next;
};

void Push(struct ListNode **top,int data){

struct ListNode *temp;
temp=(struct ListNode *) malloc(sizeof(struct ListNode));
	if(!temp)
	{
		printf("Memory Error");
		return ;
	}
	
temp->data = data;
temp->next=*top;
*top = temp;

}

int Pop(struct ListNode **top){

int element =-1;
struct ListNode *freeNode;
	
	if(IsEmptyStack(*top))
	{
		return -1;
	}	
	else{
		freeNode = *top;
		element = (*top)->data;
		*top=(*top)->next;
		free(freeNode);
	}

return element;
}

int top(struct ListNode *s){

int element =-1;
	if(IsEmptyStack(s)){
	return -1;
	}
	else{
	
	element=s->data;
	
	}
return element;
}

int IsEmptyStack(struct ListNode *root){

	if(!root)
	{
		return 1;
	}
	else{
	return 0;
	}
}

void deleteStack(struct ListNode **root){

	struct ListNode *temp;
	
	while(*root)
	{
		temp= *root;
		*root=(*root)->next;
		free(temp);
	}
	
}



main(){
struct ListNode *root=NULL;
	
	Push(&root,20);
	Push(&root,30);
	Push(&root,40);
	Push(&root,50);
	
	printf("\nTop element %d",top(root));
	printf("\nPopped Element %d",Pop(&root));
	printf("\nTop element %d",top(root));

return 0;
}
