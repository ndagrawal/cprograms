/*This program clears out all the three methods of traversal */

#include<stdio.h>
#include<stdlib.h>

/* Let us basically describe how a particular node looks in the binary tree .... Every node in the tree has three major elements , left child, right child, and  and the data. */

struct  TreeNode {
int data;
struct TreeNode *leftChild;
struct TreeNode *rightChild;
};

void inorder(struct TreeNode *bt);
void preorder(struct TreeNode *bt);
void postorder(struct TreeNode *bt);
int insert(struct TreeNode **bt,int num);

int insert(struct TreeNode **bt,int num)
{
	if(*bt==NULL)
	{
	*bt= malloc(sizeof(struct TreeNode));

	(*bt)->leftChild=NULL;
	(*bt)->data=num;
	(*bt)->rightChild=NULL;

	return;
	}
	else{
	/* */
	if(num < (*bt)->data)
	{
	insert(&((*bt)->leftChild),num);
	}
	else
	{
	insert(&((*bt)->rightChild),num);
	}
}
	return;
	}

	void inorder(struct TreeNode *bt){
if(bt!=NULL){


//Process the left node

inorder(bt->leftChild);

/*print the data of the parent node */
printf(" %d ", bt->data);

/*process the right node */


inorder(bt->rightChild);
}

}

void preorder(struct TreeNode *bt){
if(bt)
{
//Process the parent node first
printf("%d",bt->data);

//Process the left node.
preorder(bt->leftChild);

//Process the right node.
preorder(bt->rightChild);


}

}


void postorder(struct TreeNode *bt){

if(bt)
{
//process the left child 
postorder(bt->leftChild);

//process the right child
postorder(bt->rightChild);

//process the parent node 
printf("%d",bt->data);


}
}


