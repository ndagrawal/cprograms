/* 
This program helps us to find the sum of all the nodes in the iterative fashion/...
*/
#include<stdio.h>
#include<stdlib.h>
#include"LevelOrder.h"


int sumofallnodesIterative(struct TreeNode *root){

	struct TreeNode *temp;
	struct QueueList *q=NULL;
	int sum=0;
	if(!root)
	 return;
	Enqueue(&q,root);
	while(q!=NULL)
	{
		temp=Dequeue(&q);
		//processing step;
		if(temp!=NULL)
		sum = sum + temp->data; 
		
		if(temp->leftChild)
			Enqueue(&q,temp->leftChild);
		if(temp->rightChild)
			Enqueue(&q,temp->rightChild);
		
	}

return sum;
}




main()
{
int num,elements;
struct TreeNode *bt=NULL;
int i;

insert(&bt,50);
insert(&bt,40);
insert(&bt,60);
insert(&bt,35);
insert(&bt,45);
insert(&bt,55);
insert(&bt,65);


printf("In Order Traversal \n");
LevelOrder(bt);

printf("Sum of all the nodes %d",sumofallnodesIterative(bt));

return 0;
}
