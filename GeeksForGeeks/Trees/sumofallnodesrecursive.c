#include<stdio.h>
#include<stdlib.h>
#include"BasicTree.h"

int sumofallnodes(struct TreeNode *root){

	if(!root)
	{
//	printf("Memory Error");
	return 0;
	}
	
	 return sumofallnodes(root->leftChild) + root->data + sumofallnodes(root->rightChild);
	
}




main()
{
int num,elements;
struct TreeNode *bt;
int i;

insert(&bt,50);
insert(&bt,40);
insert(&bt,60);
insert(&bt,35);
insert(&bt,45);
insert(&bt,55);
insert(&bt,65);

printf("In Order Traversal \n");
inorder(bt);

printf("Sum of all nodes %d" ,sumofallnodes(bt));
return 0;
}
