/*
to check whether there exist a path with the given sum .. if exist then print the path ( extension to the real program )  */


#include<stdio.h>
#include<stdlib.h>
#include<limits.h>
#include"BasicTree.h"



int printArray(struct TreeNode *root,struct TreeNode *targetNode){

	if(!root)
	return 0;	

	if(root==targetNode || printArray(root->leftChild,targetNode)|| printArray(root->rightChild,targetNode))
	{
		printf("%d",root->data);
		return 1;
	}
	
	return 0;

}

void getTargetNode(struct TreeNode *root,struct TreeNode **targetNode,int *max_sum_ref,int curr_sum)
{
	
	if(!root)
	{
	
	return ;

	}	
	else{
		curr_sum=curr_sum + root->data;
		
		if(curr_sum > *max_sum_ref)
		{
			*max_sum_ref = curr_sum;
			*targetNode=root;
		}
 		
		getTargetNode(root->leftChild,&(*targetNode),&(*max_sum_ref),curr_sum);
		getTargetNode(root->rightChild,&(*targetNode),&(*max_sum_ref),curr_sum);		
	}

}




int printMaxSumPath(struct TreeNode *root){

	int pathArray[1000];
	struct TreeNode *targetNode ;
	int max_sum_ref=INT_MIN;
	int curr_sum=0;
//	int max=INT_MIN;

	getTargetNode(root,&targetNode,&max_sum_ref,curr_sum);
	printArray(root,targetNode);

return max_sum_ref;

}





main()
{
int num,elements;
struct TreeNode *bt=NULL;
int i;
insert(&bt,50);

insert(&bt,40);
insert(&bt,60);
insert(&bt,35);
insert(&bt,45);
insert(&bt,55);
insert(&bt,65);


printf("In Order Traversal \n");
inorder(bt);

printf("Maximum Sum path %d",printMaxSumPath(bt));

return 0;
}
