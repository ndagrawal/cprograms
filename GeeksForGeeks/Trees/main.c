/*
to check whether there exist a path with the given sum .. if exist then print the path ( extension to the real program )  */


#include<stdio.h>
#include<stdlib.h>
#include"LevelOrder.h"
#include<math.h>
#define bool int

bool CompleteOrNot(struct TreeNode *root)
{
bool check=1;
struct QueueList *Q=NULL;
struct TreeNode *temp;
struct TreeNode *r = (struct TreeNode *)malloc(sizeof(struct TreeNode));
	r->data=10001;
	r->left=NULL;
	r->right=NULL;
 
	if(!root)
	{
		return;
	}
	
	Enqueue(&Q,root);
	Enqueue(&Q,r);
int level =0;
int count =0;
	while(Q!=NULL)
	{
		temp=Dequeue(&Q);
		
		//processing steps...
		if(temp->data!=10001)
		count++;
		
		if(temp->data==10001)
		{
			if(count!=pow(2,level))
			return 0;
			
			if(Q!=NULL)
			Enqueue(&Q,r);
		level++;
		}
		
		if(temp->leftChild)
			Enqueue(temp->leftChild);
		if(temp->rightChild)
			Enqueue(temp->rightChild);
	}


	


return check;
}

main()
{
int num,elements;
struct TreeNode *bt=NULL;
int i;
insert(&bt,50);

insert(&bt,40);
insert(&bt,60);
insert(&bt,35);
insert(&bt,45);
insert(&bt,55);
insert(&bt,65);


printf("In Order Traversal \n");
inorder(bt);

printf("Complete or Not : %d",CompleteOrNot(bt));
//printHasSumPath(bt,135);

return 0;
}
