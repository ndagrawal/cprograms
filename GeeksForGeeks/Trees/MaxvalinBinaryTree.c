/*
 Program to : Insert elements in the tree.
	      Three traversals in tree inorder, postorder, preorder;
	      The max and min element of the binary tree.
	 			      
*/
#include<stdio.h>
#include<stdlib.h>
#define INT_MIN -1
/* Let us basically describe how a particular node looks in the binary tree .... Every node in the tree has three major elements , left child, right child, and  and the data. */

struct  TreeNode {
int data;
//Refering to the left child.
struct TreeNode *leftChild;
//Reffering to the right child.
struct TreeNode *rightChild;
};

void inorder(struct TreeNode *bt);
void preorder(struct TreeNode *bt);
void postorder(struct TreeNode *bt);
int insert(struct TreeNode **bt,int num);
int FindMax(struct TreeNode *bt);
int FindMin(struct TreeNode *bt);

main()
{

int num;	//Number of elements in the binary tree.
int elements;   //Actual elements in the tree.
struct TreeNode *bt; //Pointer to the node 

int i; //iterator to help us add elements in the tree.

//Input: Number of elements in the tree. 
printf("Enter number of elements to be inserted in the tree");
scanf("%d",&num);

printf("Enter the elements to be inserted inside the tree");
for(i=0;i<num;i++)
 {
 scanf(" %d",&elements);
 insert(&bt,elements);
 printf("\n");
 }

printf("\nIn Order Traversal \n");
inorder(bt);

printf("\nPre Order Traversal \n");
preorder(bt);

printf("\nPost Order Traversal \n");
postorder(bt);

printf("\nThe maximum of tree %d\n",FindMax(bt));

printf("\nThe minimum of tree %d\n",FindMin(bt));


return 0;
}

int insert(struct TreeNode **bt,int num)
{
if(*bt==NULL)
{
*bt= malloc(sizeof(struct TreeNode));

(*bt)->leftChild=NULL;
(*bt)->data=num;
(*bt)->rightChild=NULL;

return;
}
else{
/* */
if(num < (*bt)->data)
{
insert(&((*bt)->leftChild),num);
}
else
{
insert(&((*bt)->rightChild),num);
}
}
return;
}

void inorder(struct TreeNode *bt){
if(bt!=NULL){


//Process the left node

inorder(bt->leftChild);

/*print the data of the parent node */
printf(" %d ", bt->data);

/*process the right node */


inorder(bt->rightChild);
}
}

void preorder(struct TreeNode *bt){
if(bt)
{
//Process the parent node first
printf(" %d",bt->data);

//Process the left node.
preorder(bt->leftChild);

//Process the right node.
preorder(bt->rightChild);


}
}


void postorder(struct TreeNode *bt){

if(bt)
{
//process the left child 
postorder(bt->leftChild);

//process the right child
postorder(bt->rightChild);

//process the parent node 
printf(" %d",bt->data);
}
}

int FindMax(struct TreeNode *bt){

int max=-1;

int left=0;

int right=0;

int root;
	
	if(!bt)
	return 0;
	if(bt->rightChild)
	return FindMax(bt->rightChild);
	
	return bt->data;	


}


int FindMin(struct TreeNode *bt)
{
//get the minimum value of the binary tree... 
int min;
//get the minimum of the left sub-tree. 
int left=0;
//get the minimum of the right sub-tree.
int right=0;
//get the root of the current node.
int root;
    //Implementation was taken from stack overflow.... 

     if( !bt)
        return 0;  // Only if the tree contains nothing at all
    if( bt->leftChild )
        return FindMin(bt->leftChild);
    return bt->data;

}






