

#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include"BasicTree.h"

int lowestCommonAncestor(struct TreeNode *root, int data1,int data2){

	if(!root ||  root->data==data1 ||  root->data==data2)
	return -1;
	
	if(root->rightChild!=NULL && (root->rightChild->data==data1 || root->rightChild->data==data2))
	return root->data;
	
	 
	if(root->leftChild!=NULL && (root->leftChild->data==data1 || root->leftChild->data==data2))
	return root->data;
	
	if(root->data > data1 && root->data < data2) 
	return root->data;

	if(root->data > data1  && root->data > data2)
	return lowestCommonAncestor(root->leftChild,data1,data2);

	if(root->data < data1  && root->data < data2)
	return lowestCommonAncestor(root->rightChild,data1,data2);




}

int ancestorofanode(struct TreeNode *root,int data1){


	if(!root)
	return -1;
	
	if(root->data==data1 && (ancestorofnode(root->leftChild,data1) || ancestorofnode(root->rightChild,data1)))
	{
		printf("%d ",root->data);
	return 1;
	}

	return 1;
}

main()
{
int num,elements;
struct TreeNode *bt=NULL;
int i;
insert(&bt,50);

insert(&bt,40);
insert(&bt,60);
insert(&bt,35);
insert(&bt,45);
insert(&bt,55);
insert(&bt,65);


printf("In Order Traversal \n");
inorder(bt);

printf("lowest common ancestor %d",lowestCommonAncestor(bt,35,45));
//printHasSumPath(bt,135);
ancestorofanode(root,35);
return 0;
}
