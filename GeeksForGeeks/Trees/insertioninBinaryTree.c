#include<stdio.h>
#include<stdlib.h>

struct BSTNode{
int element;
struct BSTNode *left;
struct BSTNode *right;
};


void insertintoBinarySearchTree(struct BSTNode **root,int data){
	
	if(*root==NULL){
	*root=(struct BSTNode *)malloc(sizeof(struct BSTNode));
	if(!(*root)){
	printf("Memory Error");
	return;
	}
	else{
	(*root)->element=data;
	(*root)->left=(*root)->right=NULL;
	}
	
	}
	else{	
		if(data < (*root)->element)
		{
		 insertintoBinarySearchTree(&(*root)->left,data);
		}
		else{
			
		 insertintoBinarySearchTree(&(*root)->right,data);
		}	

	}
}

void inorder(struct BSTNode *root){

	if(!root)
	return;

	if(root){
	inorder(root->left);
	printf(" %d",root->element);
	inorder(root->right);
	}
}

main(){
printf("\n Binary Search Tree \n");
struct BSTNode *root=NULL;

insertintoBinarySearchTree(&root,6);
insertintoBinarySearchTree(&root,8);
insertintoBinarySearchTree(&root,2);
insertintoBinarySearchTree(&root,1);
insertintoBinarySearchTree(&root,3);
insertintoBinarySearchTree(&root,5);
inorder(root);
insertintoBinarySearchTree(&root,4);
inorder(root);

return 0;
}

