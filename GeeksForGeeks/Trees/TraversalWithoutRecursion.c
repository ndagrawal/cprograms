#include<stdio.h>
#include<stdlib.h>

struct TreeNode{
int data;
struct TreeNode *left;
struct TreeNode *right;
};

struct StackNode{
int top;
int capacity;
struct TreeNode **array;
};

	

struct StackNode *createStack(int size){

struct StackNode *newNode = ( struct StackNode *) malloc ( sizeof ( struct StackNode));

	if(!newNode)
	{
	printf("Memory Error");
	return;
	}
	newNode->top=-1;
	newNode->capacity=size;
	int i;
	newNode->array= malloc(sizeof(struct TreeNode) * newNode->capacity ) ;
	if(!newNode->array)
	{
	printf("Memory Error");
	return;
	}
	return newNode;	
	 
}
	// methods for stack exception
int IsEmptyStack(struct StackNode *Node)
{
	if(Node->top == -1 )
	{
		printf("\nStack is Empty\n");
		return 1; 
	}

return 0;

}

int IsFullStack(struct StackNode *Node)
{
	if(Node->top==Node->capacity -1 )
	{
		printf("\n Stack is Full, Stack Overflow i\n");
		return 1;
	}

return 0 ; 
}

	// push , pop operations of stack. 

void Push(struct StackNode *s,struct TreeNode *t)
{

	
	// while pushing always check that the stack is full or not .. 
	if(IsFullStack(s))
	{
		return;
	}
	else{
		// what happens here .. is first you increase the s->top by 1 and then put the data, 
		// data is put from the index zero in this case .. important corner cases as such should be remembered. 
		s->array[++s->top]=t;
		
	}
}

struct TreeNode *Pop(struct StackNode *s)
{

struct TreeNode *node=NULL ;

	if(IsEmptyStack(s))
	{
		printf("Empty Stack ");
		return ; 

	}
	else
	{
		node=s->array[s->top];
		s->top--;
	}
return node;

}

void DeleteStack(struct StackNode *s)
{
	
	if(s)
	{
		if(s->array)
		free(s->array);
		free(s);
	}
	
}

struct TreeNode *Top(struct StackNode *s){
	if(IsEmptyStack)
	{
		printf("Memory Stack");
		return;
	}
	else{
	return s->array[s->top];
	}
}
void insert(struct TreeNode **root,int data){
	
struct TreeNode *newNode;
struct TreeNode *temp=*root;


	if(!newNode)
	{
		printf("Memory Error");
	}
	
	if(*root==NULL)
	{	
		(*root)=(struct TreeNode *) malloc(sizeof(struct TreeNode));
		(*root)->data=data;
		(*root)->left=NULL;
		(*root)->right=NULL;
		//*root=newNode;
	}
	else{
		if((*root)->data > data){
		
		insert(&(*root)->left,data);
		
		}
		else
		{
		
		insert(&(*root)->right,data);
		}
		
		}
}


void PreOrderIterative(struct TreeNode *root){
	
	struct StackNode *s1=createStack(7);
	struct TreeNode *temp=NULL;
	while(1)
	{
		
	while(root)
		{
			printf("%d ",root->data);
			Push(s1,root);
			root = root->left;
		
		}
		
		if(IsEmptyStack(s1))
			break;
		root = Pop(s1);
		
		root=root->right;
		
		  
	}
	DeleteStack(s1);	
}


void inorder(struct TreeNode *root){

	
	if(root)
	{

		inorder(root->left);
		printf("%d ",root->data);		
		inorder(root->right);
	}

}

void InOrderIterative(struct TreeNode *root)
{

struct StackNode *s= createStack(7);

	while(1)
	{
		while(root)
		{
			Push(s,root);
			root= root->left;
			//printf("%d ",root->data);
		}
		if(IsEmptyStack(s))
		break;
		root=Pop(s);
		printf("%d ",root->data);
		root=root->right;
	}
}


void PostOrderIterative(struct TreeNode *root)
{
	struct StackNode *s=createStack(7);
	while(1)
	{
		while(root)
		{
			Push(s,root);
			root=root->left;
		}
		
			if(IsEmptyStack(s))
			{
				printf("Empty Stack");
				return;
			}
		
		
		 if(Top(s)->right==NULL){
			root=Pop(s);
			printf("%d ",root->data);
			if(root==Top(s)->right)
			{
			printf("%d ",Top(s)->data);
			Pop(s);
			}

		}
			
		if(!IsEmptyStack(s))
		{
			root=Top(s)->right;
			
		}
		else
		{
			root=NULL;
		}
	}
	
	DeleteStack(s);

}
main(){
struct TreeNode *root=NULL;
insert(&root,7);
insert(&root,12);
insert(&root,2);
insert(&root,5);
insert(&root,8);
insert(&root,16);
insert(&root,6);
inorder(root);
printf("\nPreOrder Traversal");
PreOrderIterative(root);
printf("\nInOrderRecursive Traversal");
InOrderIterative(root);
printf("PostOrderTraversal");
PostOrderIterative(root);
//struct StackNode *s=createStack(5);
//int check = IsEmptyStack(s);
//printf("Check %d",check);
//Push(s,root);
//Pop(s);
//Pop(s);
return 0;
}
