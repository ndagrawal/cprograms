/*
to check whether there exist a path with the given sum .. if exist then print the path ( extension to the real program )  */


#include<stdio.h>
#include<stdlib.h>
#include"BasicTree.h"

int countnumberofFullnodes(struct TreeNode *root){




	if(!root)
	return 0;
	

	if((!root->leftChild && root->rightChild)|| (root->leftChild && !root->leftChild)){
	return 0;
	} 

	if(!root->leftChild && !root->rightChild)
	return 0;	
	
		
	return 1+ countnumberofFullnodes(root->leftChild) + countnumberofFullnodes(root->rightChild);
	
		
}

int countnumberofhalfnodes(struct TreeNode *root){

	if(!root)
	return 0;
	


	if(root->leftChild && root->rightChild){
	countnumberofhalfnodes(root->leftChild);
	countnumberofhalfnodes(root->rightChild);
	}	




	if((root->leftChild && !root->rightChild) || (root->rightChild && !root->leftChild))
	{//countnumberofhalfnodes(root->leftChild);
	return 1;
	}
	
	return countnumberofhalfnodes(root->leftChild) + countnumberofhalfnodes(root->rightChild);


}


int countnumberofleafnodes(struct TreeNode *root){
	
	if(!root)
	return 0;
/*
	if(!root->leftChild && root->rightChild)
	return 0;

	if(root->leftChild && !root->rightChild)
	return 0;

*/	if(root->leftChild && root->rightChild)
	{
	 countnumberofleafnodes(root->leftChild);
	 countnumberofleafnodes(root->rightChild);
	}
	
	if(!root->leftChild && root->rightChild)
	{
	 //countnumberofleafnodes(root->leftChild);
	 countnumberofleafnodes(root->rightChild);
	}
	
	if(root->leftChild && !root->rightChild)
	{
	 countnumberofleafnodes(root->leftChild);
	 //countnumberofleafnodes(root->rightChild);
	}
	if(!root->leftChild && !root->rightChild)
	{
	 return 1;
	}
/*	if(!root->leftChild && !root->rightChild)
	return 1;
*/	
	return countnumberofleafnodes(root->leftChild) + countnumberofleafnodes(root->rightChild);

}

main()
{
int num,elements;
struct TreeNode *bt=NULL;
int i;
insert(&bt,50);

insert(&bt,40);
insert(&bt,60);
insert(&bt,45);
insert(&bt,55);
insert(&bt,65);


printf("In Order Traversal \n");
inorder(bt);

int count =0;
printf("number of full nodes: %d",countnumberofFullnodes(bt));
printf("number of half nodes: %d",countnumberofhalfnodes(bt));
printf("number of leaf nodes: %d",countnumberofleafnodes(bt));

return 0;
}
