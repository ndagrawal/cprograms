#include<stdio.h>
#include<math.h>
#include<stdlib.h>






//Recursive Function That Constructs Segment Tree for array 

int constructSegmentTreeUtil(int arr[],int StartIndex,int TailIndex,int *segmentTree,int currentNode){
//if there is one element in the array , store it in current node of the segment otree and return . 

	if(StartIndex==TailIndex){
	arr[currentNode]=arr[StartIndex];
	return arr[StartIndex];
	}

	//if there are more than one elements then recur for the left and right subtrees and store the sum of values in this node... 

int mid = StartIndex+(( TailIndex - StartIndex)/2);
segmentTree[currentNode]=constructSegmentTreeUtil(arr,StartIndex,mid,segmentTree,((currentNode*2)+1)) + constructSegmentTreeUtil(arr,mid+1,TailIndex,segmentTree,((currentNode*2)+2));

return segmentTree[currentNode];


}

/* this Function helps to allocate a memory to the segment tree from the given array . This Allocates memory for the segmenr Tree and Calls ConstructSTUTIl() and fill the allocated Memory */

int *constructSegmentTree(int arr[],int arraySize){

//allocate memory for the segment tree. 
int heightOfTree=(int)(ceil(log2(arraySize))); //Height of the segment tree.
int max_size  = 2 * (int)pow(2,heightOfTree)-1; //maximum size of the segment Tree.

int *segmentTreeRoot = malloc(sizeof(int) * max_size);	

//fill the allocated memory Segment Tree.
constructSegmentTreeUtil(arr,0,arraySize-1,segmentTreeRoot,0);

//Return the Constructed Segment Tree. 
return segmentTreeRoot;
}

main(){
int arr[]={1,3,5,7,9,11};
int arraySize=sizeof(arr)/sizeof(arr[0]);

//Build Segment Trees from the given array.
//Return the root node of the segment tree. 
int *segmentTreeRoot=constructSegmentTree(arr,arraySize);


}
