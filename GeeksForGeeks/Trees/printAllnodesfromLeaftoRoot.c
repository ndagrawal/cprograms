/*This program clears out all the three methods of traversal */

#include<stdio.h>
#include<stdlib.h>
#include "BasicTree.h"

/* Let us basically describe how a particular node looks in the binary tree .... Every node in the tree has three major elements , left child, right child, and  and the data. */



void printArray(int path[],int pathlength){
	printf("\n");
	int i;
	for(i=pathlength-1;i>=0;i--)
	{
		printf("%d ",path[i]);

	}
	printf("\n");
}

void printPathRecur(struct TreeNode *root,int path[],int pathlength){
	
	if(!root)
	return;

	path[pathlength]=root->data;
	pathlength++;
	
	if(root->rightChild==NULL && root->leftChild==NULL)
	{
	printArray(path,pathlength);
	return;
	}
	
	printPathRecur(root->leftChild,path,pathlength);
	printPathRecur(root->rightChild,path,pathlength);


}
void printAllPaths(struct TreeNode *root){

	int path[1000];
	printPathRecur(root,path,0);
}




main()
{
int num,elements;
struct TreeNode *bt=NULL;
int i;

printf("Enter number of elements to be inserted in the tree");
scanf("%d",&num);

printf("Enter the elements to be inserted inside the tree");
for(i=0;i<num;i++)
{
scanf("%d",&elements);
insert(&bt,elements);
printf("\n");
}
printf("In Order Traversal \n");
inorder(bt);

printAllPaths(bt);

return 0;
}
