#include<stdio.h>
#include<stdlib.h>

struct TreeNode {
int data;
struct TreeNode *leftChild;
struct TreeNode *rightChild;
};

struct QueueList{
struct TreeNode *treenode;
//struct QueueList *front;
struct  QueueList *rear;
};

void Enqueue(struct QueueList **head,struct TreeNode *t){
struct QueueList *newNode=(struct QueueList *) malloc(sizeof(struct QueueList));
struct QueueList *current;
current=*head;

	
	if(!newNode)
	{
		printf("Memory Error");
		return;
	}
	
	newNode->treenode=t;
	if(*head==NULL)
	{
		//newNode->treenode=t;
		
		newNode->rear=NULL;			
		(*head)=newNode;
	         	
		//*head=newNode;
				
	}else{
		while(current->rear!=NULL)
		{
			current=current->rear;
		}
		current->rear=newNode;
		
		current=current->rear;
		current->rear=NULL;
	}	
}

struct TreeNode *Dequeue(struct QueueList **front){
struct 	QueueList *temp;
	
	if(*front==NULL)
	{
		printf("Empty List");
		return;
	}	
	temp=*front;
	*front=(*front)->rear;
	
	
return temp->treenode; 
}



void insert(struct TreeNode **root,int data){
struct TreeNode *temp=NULL;

	if(*root==NULL)
	{
		*root=(struct TreeNode *)malloc(sizeof(struct TreeNode));
		if(!*root)
		{
			printf("Memory error");	
			return;
		}
		(*root)->data = data;
		(*root)->leftChild=NULL;
		(*root)->rightChild=NULL;
		
	}
	else
	{
		if((*root)->data <  data)
			insert(&(*root)->rightChild,data);
		else	
		insert(&(*root)->leftChild,data); 
	}

}



void LevelOrder(struct TreeNode *root){
	struct TreeNode *temp;
	struct QueueList *Q=NULL;
	if(!root)
		return;
	Enqueue(&Q,root);
	//printf("%d",Q->treenode->data);
	while(Q!=NULL)
	{
		temp=Dequeue(&Q);
		printf("%d ",temp->data);
		if(temp->leftChild)
		Enqueue(&Q,temp->leftChild);
		if(temp->rightChild)
		Enqueue(&Q,temp->rightChild);
	}
}



