/*
to check whether there exist a path with the given sum .. if exist then print the path ( extension to the real program )  */


#include<stdio.h>
#include<stdlib.h>
#include "BasicTree.h"

/*
I had major challenge to print the HAsh table so   I assumed that the atmost length of any leaf from the root node would be 10, as i knew that the sample tree that i am using would be of the length 4-5 atmost. 

my initial value of column starts with 10 and then its +1 and -1 as per the recursion steps... 

 */

void printHash(int hash[]){

	int i;
	for(i=0;i<=20;i++)
	{
		printf("%d ",hash[i]);

	}

	printf("\n");

}
void verticalPathSum(struct TreeNode *root,int hash[],int column)
{
	if(!root)
        {
	return;
	}
	else{
		if(root->leftChild)
		verticalPathSum(root->leftChild,hash,column-1);
	       	
	        hash[column]=hash[column] + root->data;

		if(root->rightChild)
		verticalPathSum(root->rightChild,hash,column+1);		
	}	
}
void printHasSumPath(struct TreeNode *bt,int sum)
{
	int hash[21];
	int i;
	for (i=0;i<=20;i++)
	{hash[i]=0;
	}
	int column =10;
	printHash(hash);
	verticalPathSum(bt,hash,column);
	printHash(hash);
}


main()
{
int num,elements;
struct TreeNode *bt=NULL;
int i;
insert(&bt,50);

insert(&bt,40);
insert(&bt,60);
insert(&bt,35);
insert(&bt,45);
insert(&bt,55);
insert(&bt,65);


//printf("In Order Traversal \n");
//inorder(bt);

printHasSumPath(bt,135);

return 0;
}
