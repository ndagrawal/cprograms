/*
to check whether there exist a path with the given sum .. if exist then print the path ( extension to the real program )  */


#include<stdio.h>
#include<stdlib.h>
#include"LevelOrder.h"

int numberofhalfnodes(struct TreeNode *root){
int count =0;
	struct QueueList *Q=NULL;
	struct TreeNode *temp;
	
	if(!root){
	return 0;
	}

	Enqueue(&Q,root);
	
	while(Q!=NULL)
	{

	temp=Dequeue(&Q);
	
	//processing steps...
	if(temp->leftChild && !temp->rightChild)
	count++;
	if(!temp->leftChild && temp->rightChild)
	count++;
	
	if(temp->leftChild)
	Enqueue(&Q,temp->leftChild);
	if(temp->rightChild)
	Enqueue(&Q,temp->rightChild);	
		
	}
return count;
}


int numberoffullnodes(struct TreeNode *root){
int count =0;
	struct QueueList *Q=NULL;
	struct TreeNode *temp;
	
	if(!root){
	return 0;
	}

	Enqueue(&Q,root);
	
	while(Q!=NULL)
	{

	temp=Dequeue(&Q);
	
	//processing steps...
	if(temp->leftChild && temp->rightChild)
	count++;
	
	if(temp->leftChild)
	Enqueue(&Q,temp->leftChild);
	if(temp->rightChild)
	Enqueue(&Q,temp->rightChild);	
		
	}
return count;
}


int numberofleafnodes(struct TreeNode *root){
int count =0;
	struct QueueList *Q=NULL;
	struct TreeNode *temp;
	
	if(!root){
	return 0;
	}

	Enqueue(&Q,root);
	
	while(Q!=NULL)
	{

	temp=Dequeue(&Q);
	
	//processing steps...
	if(!temp->leftChild && !temp->rightChild)
	count++;
	
	if(temp->leftChild)
	Enqueue(&Q,temp->leftChild);
	if(temp->rightChild)
	Enqueue(&Q,temp->rightChild);	
		
	}
return count;
}



main()
{
int num,elements;
struct TreeNode *bt=NULL;
int i;
insert(&bt,50);

insert(&bt,40);
insert(&bt,60);
insert(&bt,35);
insert(&bt,45);
insert(&bt,55);
insert(&bt,65);



printf("number of half nodes: %d",numberofhalfnodes(bt));
printf("\nnumber of full nodes : %d/",numberoffullnodes(bt));
printf("\nnumber of leaf nodes : %d ",numberofleafnodes(bt));

return 0;
}
