#include<stdio.h>
#include<stdlib.h>

struct TreeNode {
int data;
struct TreeNode *left;
struct TreeNode *right;
};

struct QueueList{
struct TreeNode *treenode;
//struct QueueList *front;
struct  QueueList *rear;

};

void Enqueue(struct QueueList **head,struct TreeNode *t){
struct QueueList *newNode=(struct QueueList *) malloc(sizeof(struct QueueList));
struct QueueList *current;
current=*head;

	
	if(!newNode)
	{
		printf("Memory Error");
		return;
	}
	
	newNode->treenode=t;
	if(*head==NULL)
	{
		//newNode->treenode=t;
		
		newNode->rear=NULL;			
		(*head)=newNode;
		
		//*head=newNode;
				
	}else{
		while(current->rear!=NULL)
		{
			current=current->rear;
		}
		current->rear=newNode;
		
		current=current->rear;
		current->rear=NULL;
	}	
}

struct TreeNode *Dequeue(struct QueueList **front){
struct 	QueueList *temp;
	
	if(*front==NULL)
	{
		printf("Empty List");
		return;
	}	
	temp=*front;
	*front=(*front)->rear;
	
	
return temp->treenode; 
}



void insert(struct TreeNode **root,int data){
struct TreeNode *temp=NULL;

	if(*root==NULL)
	{
		*root=(struct TreeNode *)malloc(sizeof(struct TreeNode));
		if(!*root)
		{
			printf("Memory error");	
			return;
		}
		(*root)->data = data;
		(*root)->left=NULL;
		(*root)->right=NULL;
		
	}
	else
	{
		if((*root)->data <  data)
			insert(&(*root)->right,data);
		else	
		insert(&(*root)->left,data); 
	}

}



int  Maxelement(struct TreeNode *root){
	struct TreeNode *temp;
	struct QueueList *Q=NULL;
	int max=-1;
	if(!root)
		return;
	Enqueue(&Q,root);
	//printf("%d",Q->treenode->data);
	while(Q!=NULL)
	{
		
		temp=Dequeue(&Q);
		if(temp->data > max)
			max=temp->data;
	//	printf("%d ",temp->data);
		if(temp->left)
		Enqueue(&Q,temp->left);
		if(temp->right)
		Enqueue(&Q,temp->right);
	}
return max;
}

int  Minelement(struct TreeNode *root){
	struct TreeNode *temp;
	struct QueueList *Q=NULL;
	int min=-1;
	if(!root)
		return;
	Enqueue(&Q,root);
	min=Q->treenode->data;
	//printf("%d",Q->treenode->data);
	while(Q!=NULL)
	{
		
		temp=Dequeue(&Q);
		
		if(temp->data <  min)
			min=temp->data;
	//	printf("%d ",temp->data);
		if(temp->left)
		Enqueue(&Q,temp->left);
		if(temp->right)
		Enqueue(&Q,temp->right);
	}
return min;
}
main()
{
struct TreeNode *root=NULL;

insert(&root,60);
insert(&root,24);
insert(&root,30);
insert(&root,35);
insert(&root,72);
insert(&root,65);
insert(&root,68);
insert(&root,61);
insert(&root,73);
//preOrder(root);

printf("Min element :%d",Minelement(root));

printf("Max element :%d",Maxelement(root));

return 0;
}

