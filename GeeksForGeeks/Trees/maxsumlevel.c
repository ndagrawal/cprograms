/*This program clears out all the three methods of traversal */

#include<stdio.h>
#include<stdlib.h>
#include "LevelOrder.h"
#include <limits.h>
//we are trying to find the max width using the iterative method"
/* Let us basically describe how a particular node looks in the binary tree .... Every node in the tree has three major elements , left child, right child, and  and the data. */

int maxlevelsum(struct TreeNode *root)
{

struct TreeNode *temp;
struct QueueList *q=NULL;
struct TreeNode *r =(struct TreeNode *) malloc(sizeof(struct TreeNode));
	if(!root)
	return;

int Levelsum[100];
int m;
	for(m=0;m<100;m++)
	{
	 Levelsum[m]=0;
	}
int level=0;
int sum=0;

	r->data=10001;
	r->leftChild=NULL;
	r->rightChild=NULL;
	Enqueue(&q,r);
	Enqueue(&q,root);

	while(q!=NULL)
	{
		temp=Dequeue(&q);
		
		if(temp->data!=10001)	
		Levelsum[level]=Levelsum[level] + temp->data;
		
		if(temp->data==10001)
		{	
			if(q!=NULL)
			Enqueue(&q,r);
		level++;	
		}
		else{
		if(temp->leftChild)
			Enqueue(&q,temp->leftChild);
		
		if(temp->rightChild)
			Enqueue(&q,temp->rightChild);
		}
	}


int i;
int max=INT_MIN;
	for(i=0;i<level;i++)
	{
	  if(Levelsum[i]>max)
	   {
		max=Levelsum[i];
		printf(":%d",max);   
	}
	}
	
return max;
}

main()
{
int num,elements;
struct TreeNode *bt=NULL;
int i;

printf("Enter number of elements to be inserted in the tree");
scanf("%d",&num);

printf("Enter the elements to be inserted inside the tree");
for(i=0;i<num;i++)
{
scanf("%d",&elements);
insert(&bt,elements);
printf("\n");
}


//printf("In Order Traversal \n");
//inorder(bt);
printf(" Max Width - %d ",maxlevelsum(bt));
return 0;
}
