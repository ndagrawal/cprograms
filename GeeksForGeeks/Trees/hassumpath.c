/*
to check whether there exist a path with the given sum .. if exist then print the path ( extension to the real program )  */


#include<stdio.h>
#include<stdlib.h>
#include "BasicTree.h"

void printPathArray(int pathArray[],int pathlength){

	int i=0;
	for(i=0;i<pathlength;i++)
	{
		printf("%d ",pathArray[i]);

	}

	printf("\n");

}
int HasSumPath(struct TreeNode *root,int pathArray[],int pathlength,int expectedSum)
{
	int remainingSum;
	int ans=0;	
	if(!root)
        {
	return expectedSum=0;
	}
	else{
		remainingSum=expectedSum-root->data;
		pathArray[pathlength++]=root->data;	
	       	
	       // printf("r:%d",remainingSum);	
		if(remainingSum == 0 && root->leftChild==NULL && root->rightChild==NULL)
		{	
		printPathArray(pathArray,pathlength);	
		return 1;	
		}		

		if(root->leftChild)
		ans = ans || HasSumPath(root->leftChild,pathArray,pathlength,remainingSum);
		
		
		if(root->rightChild)
		ans = ans ||  HasSumPath(root->rightChild,pathArray,pathlength,remainingSum);
		
	}	

return ans;
}
void printHasSumPath(struct TreeNode *bt,int sum)
{
	int pathArray[1000];
	int pathlength=0;
	HasSumPath(bt,pathArray,pathlength,sum);
}


main()
{
int num,elements;
struct TreeNode *bt=NULL;
int i;
insert(&bt,50);

insert(&bt,40);
insert(&bt,60);
insert(&bt,35);
insert(&bt,45);
insert(&bt,55);
insert(&bt,65);


//printf("In Order Traversal \n");
//inorder(bt);

printHasSumPath(bt,135);

return 0;
}
