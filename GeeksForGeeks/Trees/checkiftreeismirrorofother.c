/*
to check whether there exist a path with the given sum .. if exist then print the path ( extension to the real program )  */


#include<stdio.h>
#include<stdlib.h>
#include "BasicTree.h"


int checkifidentical(struct TreeNode *root1,struct TreeNode *root2){


	if(root1==NULL && root2==NULL)	
	return 0;

	if(root1->leftChild && !root2->leftChild)
	return 0;
	
	if(root1->rightChild && !root2->rightChild)
	return 0;

	if((root1->leftChild && root2->leftChild) || (root1->rightChild && root2->rightChild))
	return ((root1->data==root2->data) &&  (checkifidentical(root1->leftChild, root2->leftChild) && checkifidentical(root2->rightChild,root2->rightChild)));

}	

int checkifmirrorTree(struct TreeNode *root1,struct TreeNode *root2){


	if(root1==NULL && root2==NULL)	
	return 0;

	if(root1->leftChild && !root2->rightChild)
	return 0;	
	
	if(root1->rightChild && !root2->leftChild)
	return 0;

	if((root1->leftChild && root2->rightChild) || (root1->rightChild && root2->leftChild))
	return ((root1->data==root2->data) &&  (checkifmirrorTree(root1->leftChild, root2->rightChild) && checkifmirrorTree(root2->leftChild,root2->rightChild)));
}

int checkifisomorphic(struct TreeNode *root1,struct TreeNode *root2){


	if(root1==NULL && root2==NULL)	
	return 0;

	if(root1==NULL || root2==NULL)
	return 0;

	if(root1->data!=root2->data)
	return 0;
// There are two possible cases for n1 and n2 to be isomorphic
 // Case 1: The subtrees rooted at these nodes have NOT been "Flipped".
 // Both of these subtrees have to be isomorphic, hence the &&
 // Case 2: The subtrees rooted at these nodes have been "Flipped"	

	return  (checkifisomorphic(root1->leftChild, root2->rightChild) && checkifisomorphic(root2->leftChild,root2->rightChild)) &&
	(checkifisomorphic(root1->leftChild, root2->leftChild) && checkifisomorphic(root2->rightChild,root2->rightChild));
}
main(){

int num,elements;
struct TreeNode *bt1=NULL;
struct TreeNode *bt2=NULL;
int i;
insert(&bt1,50);
insert(&bt1,40);
insert(&bt1,60);
insert(&bt1,35);
insert(&bt1,45);
insert(&bt1,55);
insert(&bt1,65);

insert(&bt2,50);
insert(&bt2,40);
insert(&bt2,60);
insert(&bt2,35);
insert(&bt2,45);
insert(&bt2,55);
insert(&bt2,65);


printf("Check if the tree is mirror  %d",checkifmirrorTree(bt1,bt2));

printf("Check if the tree is isomorphic...                                  %d",checkifisomorphic(bt1,bt2));
printf("Check if the tree is identical  %d",checkifidentical(bt1,bt2));
return 0;
}
