#include<stdio.h>
#include<stdlib.h>

struct ListNode {
int data;
struct ListNode *next;
};


void insertNodes(struct ListNode **head,int data,int position){
struct ListNode *current;
current=*head;
struct ListNode *newNode;
struct ListNode *prev;
int k=1;
	
	newNode=(struct ListNode *) malloc(sizeof(struct ListNode));
	if(!newNode)
	{
	printf("Memory Error");
	return;
	}
	newNode->data=data;
	if(position==1)
	{
		newNode->next=current;
		*head=newNode;	
	}
	else if(position >1)
	{
		while(k<position && current->next!=NULL)
		{
			prev=current;
			current=current->next;
			k++;
		}
	
		if(current->next!=NULL)
		{
			newNode->next=current->next;
			prev->next=newNode;
		}
		else
		{
			newNode->next = NULL;
			current->next=newNode;		
		}
	}

}
struct ListNode *findnthnode(struct ListNode *head,int n){

int count=0;
	if(!head)
	{
	printf("Empty List");
	return;
	}


	while(head)
	{
		count++;
		if(count==n)
		{
	  	 return head;
		}
		head=head->next;
	}

	
return NULL;
}

int ListLength(struct ListNode *head)
{
int count=0;
	while(head!=NULL)
	{
		count++;
		head=head->next;
	}
return count;
}
struct ListNode *findnthnodefromend(struct ListNode *head,int n){
//approach no 1 : directly using M-n+1

int Length;
struct ListNode *current;
current=head;
Length=ListLength(head);
int nodeposition = Length - n +1 ;
	if(nodeposition < 1)
	{
	printf("Such a position does not exists ");
	return;
	}
	while(current!=NULL)
	{
		nodeposition--;
		if(nodeposition==0)
		{
	 	return current;
		}
		
		current=current->next;
	}
return NULL;
}

struct ListNode * findnthNodefromLast2(struct ListNode *head,int n)
{
struct ListNode *mainptr=head;
struct ListNode *nthptr=head;
	
int count=1;
	if(!head)
	{
		printf("Empty LinkedList");
		return;
	
	}
	while(mainptr!=NULL && count <n)
	{
	 count++;

	 printf("%d",mainptr->data);
	 mainptr=mainptr->next;
	}

	while(mainptr!=NULL)
	{
	 nthptr=nthptr->next;
	 mainptr=mainptr->next;
	}
	
	if(nthptr)
	{
		return nthptr;
	}


}

void findnthNodefromLast3(struct ListNode *head,int n)
{
struct ListNode *nthnode;
	int i=0;
	if(head==NULL)
	{	
		return ;
	}
	findnthNodefromLast3(head->next,n);
	i++;
	if(i==n)
	{
	 printf("%d",head->data);
	 return;
	}
}

void findmiddleelement1(struct ListNode *head)
{
	struct ListNode *slowptr=head;
	struct ListNode *fastptr=head;
	
	/*
	move fastptr by two step. 
	move slowptr by one step. 
	*/

	while(fastptr->next!=NULL){
		
		fastptr=fastptr->next->next;
		slowptr=slowptr->next;
	}	
	

	if(slowptr)
	{
	printf("%d",slowptr->data);
	}
}

void findmiddleelemtent2(struct ListNode *head){
	
	struct ListNode *main_ptr=head;
	struct ListNode *ref_ptr=head;
	int count=0;
	
	/*
	move main_ptr always by one step and move slw_ptr only by one step....
	*/

	while(main_ptr!=NULL)
	{
	count++;
	if(count & 1)
	{
	ref_ptr=ref_ptr->next;
	}
	main_ptr=main_ptr->next;
	}

	printf("%d",main_ptr->data);
}


int findLength(struct ListNode *head){
int count=0;
	
	while(head!=NULL)
	{
	count++;
	head=head->next;
	}	

return count;
}

void printLinkedList(struct ListNode *head){

	while(head)
	{
	 printf("%d",head->data);
	 head=head->next;
	}	
}

main()
{
struct ListNode *head=NULL;
	insertNodes(&head,2,1);
	insertNodes(&head,3,2);
	insertNodes(&head,2,3);
	insertNodes(&head,1,4);
	insertNodes(&head,3,5);

	printf("Linked List \n");
        printLinkedList(head);	
	printf("Linked List \n");

	
struct ListNode *nthNode=findnthnode(head,2);
	printf(" node at 2 is. \n");
	printf("%d",nthNode->data);
	printf("3rd node from end\n");
nthNode=findnthnodefromend(head,3);
	printf("%d",nthNode->data);
	printf("middle element\n");
	findmiddleelement1(head);

printf("\n Length = %d",findLength(head));


	
return;
}
