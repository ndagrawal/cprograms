#include<stdio.h>
#include<stdlib.h>

struct DLLNode{
int data;
struct DLLNode *prev;
struct DLLNode *next;
};


void DLLInsertion(struct DLLNode **node,int data,int position);
void DLLDeletion(struct DLLNode **node,int position);
int DLLSearching(struct DLLNode *node, int data);
void printDLL(struct DLLNode *head);
int DLLcount(struct DLLNode *head);

main()
{
struct DLLNode *head=NULL;
int element=0;
DLLInsertion(&head,2,1);
DLLInsertion(&head,3,2);
DLLInsertion(&head,4,3);
DLLInsertion(&head,1,2);
printDLL(head)	;
DLLDeletion(&head,2);
printf("After Deleting");
printDLL(head);
printf("Element to be searched ");
scanf("%d",&element);
printf("Position :: %d",DLLSearching(head,element));

return 0;
}

void DLLInsertion(struct DLLNode **head,int data,int position){
struct DLLNode *current,*newNode,*prev;
int k=1;
current=*head;
newNode=(struct DLLNode *) malloc(sizeof(struct DLLNode));

	if(!newNode)
	{
	printf("Memory Error");
	return ;	
	}

	newNode->data=data;
	if(current==NULL && position==1)
	{
		newNode->prev=NULL;
		newNode->next=NULL;
		*head=newNode;
	}
	else if(position > 1)
	{
		while(k<position-1 && current->next !=NULL)
		{	k++;
			current=current->next;
		}
		
		if(current->next!=NULL)
		{
		//insertion of middle element... 
		newNode->next=current->next;
		newNode->prev=current;
		current->next=newNode;
		}	
		else
		{	
		//insertion of last element.....
		newNode->next=NULL;
		newNode->prev=current;
		current->next=newNode;
		}
	
	}



}

void printDLL(struct DLLNode *head)
{
	if(!head)
	{
	printf("Empty linked list ");	
	return;
	}
	while(head!=NULL)
	{
		printf("%d ",head->data);
	 	head=head->next;
	}

}
void DLLDeletion(struct DLLNode **head,int position){
	
	struct DLLNode *current;
	current = *head;
	struct DLLNode *temp;
	int k=1;

	if(!current)
	{
	printf("Empty Linked List ");
	return;
	}
	
	if(current!=NULL && position ==1)
	{
		
		temp=current;
		current->next->prev=NULL;
		*head=current->next;
		free(current);
		return;
	}
	else if(position>1)
	{	
	while(current!=NULL && k<position)
	{
		k++;	
		current=current->next;
	}
	
	if(current->next!=NULL)
	{	
		temp=current;
		current->next->prev=current->prev;
		current->prev->next=current->next;
		free(temp);

	}
	else
	{
		//current->next==NULL....
		temp=current;
		current->prev->next=NULL;
		free(temp);
	}
	
	}

}

int DLLSearching(struct DLLNode *head,int data)
{
	int position=0;
	//temp=NULL;
	while(head!=NULL)
	{	
		position++;
		if(head->data==data)
		{
		return position;
		}

		head=head->next;
	}	
	
	return -1;
}


