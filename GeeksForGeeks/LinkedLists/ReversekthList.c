#include<stdio.h>
#include<stdlib.h>

struct node{
int data;
struct node *next;
};

void append(struct node **s,int data);
void display(struct node *s);
void delete(struct node **s);
void search(struct node *s,int data);	
void reverse(struct node *s);
void insert(struct node **s,int position);
void remov(struct node *s,int data);
void addatbeg(struct node **s,int data);
/*
This function helped to reverse the consecutive K nodes .. 
 
*/
struct node *ReverseKth(struct node *s,int data);
/*
This function helps to reverse the alternate k nodes of the linked Lists .. 
 
*/

struct node *ReverseAlternateKth(struct node *s,int data);

main()
{
int i;
struct node *s=NULL;

//insert an element in the last .. 
append(&s,1);
//display(s);
append(&s,2);
//display(s);
append(&s,3);
//display(s);
append(&s,4);
//display(s);
append(&s,5);

append(&s,6);
append(&s,7);
append(&s,8);
append(&s,9);
append(&s,10);
append(&s,11);
//display 

//addatbeg(&s,3);
display(s);
//insert an element in the node ... 
//insert(&s,3);

//reverse the link list...... 
//reverse(s);

//displays all the element of the link list.
//display(s);
/*
   delete(&s);
   display(s);
   reverse(s);
   display(s);
 */
s=ReverseKth(s,2);
printf("\n");
display(s);

printf("\n");
s=ReverseAlternateKth(s,3);
display(s);
return 0;
}

void append(struct node **s,int data){

//node in which we will store the data .. 
//struct node *newNode=malloc(sizeof(struct node));
//temp node to help us to traverse... 
struct node *temp;
temp=*s;


if(*s==NULL){
*s=malloc(sizeof(struct node));
temp=*s;

}
else
{
	while(temp->next!=NULL){
		temp=temp->next;
	}

//produce a node... . 
temp->next=malloc(sizeof(struct node));
//go ahead... 
temp=temp->next;

temp->data =  data;
temp->next = NULL;

return;
}


}

void addatbeg(struct node **s,int data)
{
 struct node *temp;
	temp=malloc(sizeof(struct node));
	
	temp->data=data;
	temp->next=*s;
	*s=temp;
	

}



void display(struct node *s){

 
 if(s==NULL)
  {
   printf("List does not exis");
}
else{
	while(s->next!=NULL)
	{
	printf("%d",s->data);
	s=s->next;
	}
printf("%d",s->data);
}}

struct  node *ReverseKth(struct node *head,int k)
{
int count=0;
struct node *current;
struct node *prev=NULL;
struct node *next=NULL;
	current=head;
	
	while(!current)
	{
	printf("Memory Error");
	return;
	}
	
	while(count<k && current!=NULL){
	
//	if(current->next){
	
	next=current->next;
	current->next=prev;
//	}
	prev=current;
//	if(current->next)
	current=next;
	count++;
	}

	if(next){
	printf("next %d",next->data);
	head->next=ReverseKth(next, k);
	}
	 // the last prev is the head .. so returning the head .. in a way.
return prev;	
}

/*
 
Input  : 1->2->3->4->5->6->7->8->9->NULL; and k=3
Output : 3->2->1->4->5->6->9->8->7->NULL 

Note that alternate K nodes are reversed ,... hence 1,2,3 are reversed and 4,5,6 are not reversef ,... and then 7,8,9 are reversed.


*/




struct node *ReverseAlternateKth(struct node *s,int k){
struct node *current;
struct node *prev=NULL;
struct node *next;
struct node *headprev=NULL;
	int count =0;
	current=s;
		if(!current)
		{
			return;
		}
		
		/*Reversing the First K nodes .. */

		while(current!=NULL  && count <k)
		{	
			next=current->next;
			current->next=prev;
			prev=current;
			current=next;
			count++;
		} 
			
		headprev=prev;
		count =0;
		if(!current)
		return;

		while(prev->next!=NULL && count<k)
		{
		printf("Prev : %d",prev->data);
		prev=prev->next;
		count++;
		}
	
		if(prev->next==NULL)	
		prev->next=current;
		/*Traversing the next 2K nodes fromt the previous .. ... */
	
		count =0;
		while(current!=NULL && count <k)
		{	prev=current;
			printf("Current : %d ",current->data);
			current=current->next;
			count++;			
		}
				
		if(current){
		printf("current : %d",current->data);
		prev->next=ReverseAlternateKth(current,k);
		}
return headprev;

}
