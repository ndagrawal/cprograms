#include<stdio.h>
#include<stdlib.h>
/*
other approach  : 
-> detect if the loop is present .. 
-> find where the loop is ... 
-> count the length of loop....
-> remove the loop ... 

*/

struct ListNode{
int data;
struct ListNode *next;
};

void insertNode(struct ListNode **head,int data,int position){
struct ListNode *current=*head;
struct ListNode *prev;
struct ListNode *newNode;
newNode=(struct ListNode *)malloc(sizeof(struct ListNode));
int k=1;	
	if(!newNode)
	{
	printf("memory node ");
	return;
	}
	
	newNode->data=data;
	if(position==1 && current ==NULL)
	{
	newNode->next=current;
	
	*head=newNode;
	current=*head;
	}
	else if(position>1 && current!=NULL)
	
		{
		while(k<position-1 &&current->next!=NULL){
		prev=current;
		current=current->next;
		k++;
		}
		if(current->next!=NULL)
		{
			newNode->next=current;
			prev->next=newNode;
		}
		else
		{
			newNode->next=NULL;
		current->next=newNode;
		}
	}
		

}

int IsLoopDetected(struct ListNode *head){

struct ListNode *slowptr=head;
struct ListNode *fastptr=head;
	while(slowptr && fastptr){
	fastptr=fastptr->next;
	if(fastptr==slowptr)
	{
		return 1;
	}
	
	if(fastptr==NULL)
	{
		return 0;
	}
	fastptr=fastptr->next;
	if(fastptr==slowptr)
	{
		return 1;
	}
	slowptr=slowptr->next;
	}

return 0;
}

struct  ListNode *loopPoint(struct ListNode *head,int loopstatus){
	
struct ListNode *slowptr=head;
struct ListNode *fastptr=head;
	
	slowptr=head;
	while(slowptr!=fastptr)
		{
			fastptr=fastptr->next;
			slowptr=slowptr->next;
		}
		return slowptr;		
	
}
main()
{
struct ListNode *head=NULL;
insertNode(&head,2,1);
insertNode(&head,1,2);
insertNode(&head,4,3);
insertNode(&head,5,4);
insertNode(&head,6,5);
insertNode(&head,7,6);

head->next->next->next->next->next->next=head->next->next;
int loopstatus=IsLoopDetected(head);
printf("Loop Detected : %d",loopstatus);

head=loopPoint(head,loopstatus);
printf("%d",head->data);


return;
}
