#include<stdio.h>
#include<stdlib.h>

struct CLLNode{
	int data;
	struct CLLNode *next;
	};

void CLLInsertion(struct CLLNode **head,int data);
void CLLDelete(struct CLLNode **head,int position);
void printCLL(struct CLLNode *head);
int countCLL(struct CLLNode *head);
main()
{
	struct CLLNode *head=NULL;
	CLLInsertion(&head,2);
	CLLInsertion(&head,3);
	CLLInsertion(&head,4);
	CLLInsertion(&head,5);
	printCLL(head);
	printf("%d",countCLL(head));
	CLLDelete(&head,2);
        printCLL(head);
	return;
}
void CLLDelete(struct CLLNode **head,int position){
struct CLLNode *current=*head;
struct CLLNode *endpoint=*head;
struct CLLNode *prev;
struct CLLNode *temp;
int k=0;
	if(!head)
	{
	printf("This is empty linked lists ");
	return;
	}

	if(position==1 && current!=NULL)
	{
		//traverse the list once and find the end ... 
		do{
		   prev=current;
		   current=current->next;
		   }while(current!=*head);	
		temp=current;
		prev->next=current->next;
		current=current->next;
	        *head=current;	
		free(temp);	
	}
	else if (position>1)
	{	

		//traverse to that node ... 
		do{
			prev=current;
			current=current->next;
			k++;
		}while(current!=*head && k<position-1);	
		if(current!=*head)
		{	temp=current;
			prev->next=current->next;
			free(temp);
		}
		else
		{
			temp=current;
			prev->next=current->next;
			current=current->next;
			*head=current;
			 free(temp);
		}
		
		
	}


}
void CLLInsertion(struct CLLNode **head,int data){
struct CLLNode *newNode;
struct CLLNode *current=*head;
	
	newNode=(struct CLLNode *) malloc(sizeof(struct CLLNode));
	if(!newNode)
	{
	printf("Memory Error");
	return;
	}
	

	newNode->data=data;	
	newNode->next=newNode;
	if(*head==NULL)
	{
		*head=newNode;
	}
	else
	{
		
	while(current->next!=*head)
	{
	current=current->next;
	}	
		newNode->next=*head;
		current->next=newNode;
		
		//if u want to add element at the start.... 
		*head=newNode; 
	}
	
}

void printCLL(struct CLLNode *head)
{
	struct CLLNode *endPoint=head;
	printf("CLL Node ");
	if(!head)
	{
	printf("Empty LinkedList ");
	return;
	}

	do{		
	printf("%d",head->data);
	head=head->next;
	}while(head!=endPoint);
	
	
}

int  countCLL(struct CLLNode *head){
int count=0;
	struct CLLNode *endPoint=head;
	printf("Counting the linked List");
	if(!head)
	{
	printf("Memory Error");
	return;
	}
	do{
	count++;
	head=head->next;
	}while(head!=endPoint);	

return count;
}
