#include<stdio.h>
#include<stdlib.h>


struct ListNode{
int data;
struct ListNode *next;
};

//insertion ... 
void singlyLinkedListInsertion(struct ListNode **node, int data, int position);

//deletion ... 
void singlyLinkedListDeletion(struct ListNode **node);

int  searchingLinkedList(struct ListNode *node, int data);

void reverseLinkedList(struct ListNode **node);

int countLinkedListNodes(struct ListNode *node);

void printLinkedListNode(struct ListNode *node);

int OddorEvenLength(struct ListNode *node);
main()
{

struct ListNode *head=NULL;
int element;
int oddOrEven;
	singlyLinkedListInsertion(&head,2,1);
	singlyLinkedListInsertion(&head,3,2);
	singlyLinkedListInsertion(&head,3,1);
	struct ListNode *current=head;	
	while(current!=NULL)
	{
		printf("The element is %d ",current->data);
		current=current->next;	
	}

	printLinkedListNode(head);
	printf("%d ",countLinkedListNodes(head));		
	printf("Enter the linked List u want to search ");
	scanf("%d",&element);
	printf("position :: %d", searchingLinkedList(head,element));	
		
	printLinkedListNode(head);
	oddOrEven=OddorEvenLength(head);
	printf(" %d",oddOrEven);
return 0;
}


int OddorEvenLength(struct ListNode *node){
struct ListNode *current=node;
	while(current && current->next)
	{
		current=current->next->next;
		
	}

	if(!current)
	{
	printf("Length of Link List is Even " );
	return 0;
	}
	
	if(current)
	{
	printf("Length of Linked List is odd ");
	return 1;
	}
}

//insertion... 
void singlyLinkedListInsertion(struct ListNode **head,int data,int position){
struct ListNode *newNode,*current,*prev;
int k=1;
//printf("%d",data);
 //	reverseLinkedList(&head);
//asssinging the head pointer to link list to current pointer.
current=*head;
newNode=(struct ListNode *) malloc(sizeof(struct ListNode));
	
	if(!newNode){
		printf("Memory Error ");
		return;
	}
		
	newNode->data=data;
	if( position ==1)
	{
		//inserts at the begining ... 
		newNode->next=current;
		*head=newNode;
	}
	else if (current !=NULL && position >1)
	{
		//traverse till the middle of the upto the middle of linked Lists ... 
		while( k < position &&	current->next!=NULL)
		{
			prev=current;
			current=current->next;
			k++;	
		}
		
		//middle of linked list ... 		
		if(current->next!=NULL)
		{
			newNode->next=current;
			prev->next=newNode;	
					
		}
		else
		{//end of the linked List ... 	
			current->next=newNode;
			newNode->next=NULL;
				
		}

	}

}

int searchingLinkedList(struct ListNode *head,int data){
int position=0;
	
	while(head!=NULL) 
	{
		//head=head->next;
		position++;

		if(head->data==data)
		{
			return position;
		}
		head=head->next;
	}

return -1;
}

//as we are changing the head pointer hence we are using double pointer.. 
//reverse the linked Lists.

void reverseLinkedList(struct ListNode **head){
struct ListNode *prev=NULL;
struct ListNode *current=*head;
struct ListNode *next;

	if(!current)
	{
	printf("this is it ... empty link list bhej diya... ");
	return;
	}	
	
	if(current!=NULL)
	{
		next=current->next;
		current->next=prev;
		prev=current;
		current=next;
	}
	*head=prev;
	

}

// as we have to return the count so ... 
int  countLinkedListNodes(struct ListNode *head){
	
	int count=0;
	printf("Count Linked Lists ...  \n");
	while(head!=NULL)
	{
	count++;
	head=head->next;		
	
	}

return count;
}

void printLinkedListNode(struct ListNode *head){
struct ListNode *current ;
current = head;	
	printf("LinkedList \n" );
	while(current!=NULL)
	{
		printf("%d ",current->data);
		current=current->next;
	
	}
}


