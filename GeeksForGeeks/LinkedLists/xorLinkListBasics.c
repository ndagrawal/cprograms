/*

Xor Based implementation of linked lists : 
this can help us to move in both directions forward and backward with only one pointer which stores the XOR of previous and next node instead of storing address. 

In this program we are just doing the insertion only at the begining. 


Further Scope : 

1. The insertion at middle, or at the end 
2. Deletion operation in the node 
3. Reversing operation
4. Can we do circular linked list using Xor 
 
 
*/

#include<stdio.h>
#include<stdlib.h>

/*
 Note that following node does not have prev and next pointers but has only data and XorNode= xor of prev and next node. 
*/
struct xorNode{
int data;
struct xorNode *npx; //result of xor of prev and next node is stored here.
};


//printing the linked list in forward direction towards right :) 
void printXorList(struct xorNode *head);

struct xorNode *xoroperation(struct xorNode *node1,struct xorNode *node2);

//help us insert the node...this inserts the node only at the begining ... 
void insertXor(struct xorNode **head,int data);

//this will help us know how we can traverse ahead and then backwards :) 
void printXorListBackwards(struct xorNode *head);

main()
{
struct xorNode *head=NULL;
	
	insertXor(&head,2);
	insertXor(&head,3);
	insertXor(&head,4);
	insertXor(&head,5);
	insertXor(&head,6);
	insertXor(&head,7);
	insertXor(&head,8);

	printf("Priting Forward direction ");

	printXorList(head);

	printf("Printing Backwards now ");

	printXorListBackwards(head);


return;
}


//printing the linked list in forward direction towards right :) 
void printXorList(struct xorNode *head){
struct xorNode *current;
current=head;
struct xorNode *prev=NULL;
struct xorNode *next;

	if(!head)
	{
	printf("Empty Linked List");
	return;
	}
	
	while(current!=NULL)
	{
	printf("%d ",current->data);
	/*
	to move ahead do the xor operation with the previous node and current node */
	
	next =xoroperation(prev,current);
	prev  = current;
	current = next;
	}

}


struct xorNode *xoroperation(struct xorNode *node1,struct xorNode *node2){
struct xorNode *result;

	/*
		we are first converting the node1 and node 2 to unsigned int , so as to support xor operation (^)
		then we are type casting it to struct xorNode *

	*/
	return (struct xorNode *)((unsigned int)(node1) ^ (unsigned int)(node2));

}

//help us insert the node...this inserts the node only at the begining ... 
void insertXor(struct xorNode **head,int data){
struct xorNode *current;
current = *head;
struct xorNode *newNode;
struct xorNode *next;


 	newNode=(struct xorNode *)malloc(sizeof(struct xorNode));
	
	if(!newNode)
	{
	printf("Memory Error");
	return;
	}	

	newNode->data=data;

	/*
	
	as we are inserting the node at the first position.
	important insertion step....

	*/
	
	newNode->npx=xoroperation(current,NULL);	
	
	/*
	
	changing all the next pointers of all the nodes... 
	note that adding a node can affect next pointers of the next node .
 
	*/
	if(*head!=NULL)
	{
		next=xoroperation(current->npx,NULL);		
		current->npx=xoroperation(newNode,next);	
	}

	*head=newNode;
	
}

//this will help us know how we can traverse ahead and then backwards :) 
void printXorListBackwards(struct xorNode *head){

	
}


