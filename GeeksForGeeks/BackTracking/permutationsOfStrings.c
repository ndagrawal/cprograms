#include<stdio.h>
#include<stdlib.h>

void swap(char *x,char *y)
{
char temp;

	temp = *x;
	*x   = *y;
	*y   = temp;

}
void permute(char str[],int i,int n){

int j;

	if(i==n)
	printf("%s \n",str);

	for(j = i;j <= n;j++)
	{
		swap((str + i),(str + j));
		permute(str,i+1,n);
		swap((str + i),(str + j));
	}
}

main(){
char str[]="ABC";
permute(str,0,2);
return 0;
}
