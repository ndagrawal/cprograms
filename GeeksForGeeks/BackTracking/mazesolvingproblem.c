#include<stdio.h>
#include<stdlib.h>

#define mazerow 8
#define mazecol 8

int isSafe(int x,int y,int maze[mazerow][mazecol],int solVector[mazerow][mazecol]){
	
	
	if( x >= 0 && x < mazerow  && y>=0 && y<mazecol && maze[x][y]==1)
	{	
	//solVector[x][y]=1;
	return 1;
	}

return 0;	
}


void printSol(int solVector[mazerow][mazecol]){
int i,j;
	
	for(i=0;i<mazerow;i++)
	{ 
	 for(j=0;j<mazecol;j++)
	  {
		printf(" %d",solVector[i][j]);
	  }
	 	printf("\n");
	}
}

int solMazeUtil(int x, int y,int maze[mazerow][mazecol],int solVector[mazerow][mazecol]){
	
	if(x == mazerow-1 && y ==mazecol-1){
		solVector[x][y] =1;
	return 1;
	}
	
	int i;	
	if(isSafe(x,y,maze,solVector))	
	{

		solVector[x][y] =1;
		if(solMazeUtil(x+1,y,maze,solVector)==1)
	  	return 1;
		
		if(solMazeUtil(x,y+1,maze,solVector)==1)
		return 1;

		solVector[x][y]=0;
		return 0;
	}
	
return 0;
}
 
void solMaze(int maze[mazerow][mazecol])
{
	
	int solVector[mazerow][mazecol];
	int i,j;

	for(i=0;i<mazerow;i++)
	{ 
 	 for(j=0;j<mazecol;j++){ 	
		solVector[i][j]=0;
	 }
        }	

	
	if(solMazeUtil(0,0,maze,solVector)==0){
	printf("Solution does not exist");
	return;
	}
	else{
		printSol(solVector);
	}
} 

main(){
int maze[mazerow][mazecol]={        {1,0,0,0},
			{1,1,0,1},
			{0,1,0,0},	
			{0,1,0,0}
		};
	
solMaze(maze);
return 0;
}
