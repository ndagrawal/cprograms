#include<stdio.h>
#include<stdlib.h>
#define N 8

int isSafe(int x,int y,int sol[N][N]){

	if(x>=0 && x < N && y>=0 && y< N && sol[x][y]==-1)
		return 1;
	
return 0;
}

void printSolVector(int sol[N][N]){
int i,k;

	for(i=0;i<N;i++)
	{
	  for(k=0;k<N;k++)
		{
			printf(" %d",sol[i][k]);
		}
		printf("\n");
	}
}

int SolveKTUtil(int x,int y,int imove,int sol[N][N],int xmove[N],int ymove[N]){
	
	int k,next_x,next_y;

	if(imove == N*N)
		return 1;

	//this will help us in trying all the routes.. 

        for(k=0;k<N;k++)
	{
		next_x = x + xmove[k];
		next_y = y + ymove[k];

		//sol[next_x][next_y] = imove;

		if(isSafe(next_x,next_y,sol)){
		sol[next_x][next_y]=imove;
		if(SolveKTUtil(next_x,next_y,imove+1,sol,xmove,ymove)==1)
		{
			return 1;
		}
		else{
		sol[next_x][next_y]=-1; //backtracking step .. 
		}
		}	
	}
return 0;
}


int SolveKT(){

	int i,j;
	int sol[N][N];
	/*initialisation of solution matrix .. */

	for(i=0;i<N;i++)
	   for(j=0;j<N;j++)
		sol[i][j]=-1;

	/*now let us define our next moves in x and y dimension */
	int xmove[8]={2, 1, -1, -2, -2, -1,  1,  2};
	int ymove[8]={1, 2,  2,  1, -1, -2, -2, -1 };

	sol[0][0]=0;
	if(SolveKTUtil(0,0,1,sol,xmove,ymove)==0)
	{
		printf("Solution Does not exist");
		return 0;
	}
	else{
	printSolVector(sol);
	}

return 1;
}

main(){

//call the function thats it .. 
SolveKT();
return 0;
}

