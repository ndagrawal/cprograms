#include<stdio.h>
#include<stdlib.h>

#define MAX 100000

void printPairs(int arr[],int arr_size,int sum){
int i,temp;
int binMap[MAX] = {0};

	for(i=0;i<arr_size;i++){
	temp = sum - arr[i];
	if(temp >= 0 && binMap[temp]==1){
	printf("Pair with the given sum %d is (%d,%d).",sum,arr[i],temp);
	
	}
	binMap[arr[i]] = 1;
	}

}

int main(){
int A[]={1,4,45,6,10,8};
int n = 16;
int arr_size = 6;

	printPairs(A,6,n);

printf("\n");
return 0;
}

