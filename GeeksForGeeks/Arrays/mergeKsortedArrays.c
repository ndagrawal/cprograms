#include<stdio.h>
#include<stdlib.h>

struct Heap{
	int *array; // dynamic array .. 
	int count;
	int capacity;
	int heap_type; // if heap_type =0 -- min heap .. heap_type = 1 -- max heap.. 

};

struct Heap *createHeap(int heap_size,int heap_type){

	struct Heap *h = (struct Heap*) malloc(sizeof(struct Heap));
	
	if(!h){
	printf("Memory Error");
	return NULL;
	}

	h->capacity = heap_size;
	h->count = 0;
	h->array=(int *)malloc(sizeof(int) * h->capacity);

	if(!h->array)
	{
	printf("Memory Error");
	return NULL;
	}

	return h;
}

int  leftChild(struct Heap *h,int i){

	int left ;
	left = (2 * i) + 1 ;
	if(left >= h->capacity)
		return -1;
	
return left;
}


int rightChild(struct Heap *h,int i){
	int right;
	right = (2 * i)+ 2;

	if(right >= h->capacity)
		return -1;

return right;
}

void percolateDown(struct Heap *h,int i){

 int l = leftChild(h,i);
 int r = rightChild(h,i);
 int min ;
 int temp;

	if(l!=-1  && h->array[l] < h->array[i]){
		min = l;
		}else
		{
		min = i;
		}
	if(r!=-1 && h->array[r] <  h->array[min])
		{
			min =  r;
		}

	if(min!=i){
		temp = h->array[i];
		h->array[i] = h->array[min];
		h->array[min] = temp;	
		percolateDown(h,min);
	}

}

// for building the heap , we have to give the pointer to the root of the heap... 
// the array ..
// and the size of the array ... 

void resize(struct Heap *newHeap){

	int *old_array= newHeap->array;

	newHeap->array = (int *) malloc(sizeof(int) * newHeap->capacity *2);
	
	if(!newHeap->array){
	printf("Memory Error");
	return;
	}

int i;
	for(i=0;i < newHeap->capacity;i++)
	 newHeap->array[i]= old_array[i];	
	
	newHeap->capacity = newHeap->capacity * 2;
			
}
void BuildHeap(struct Heap *newHeap,int A[],int n){

	if( n > newHeap->capacity)
		resize(newHeap);

	int i;
					
	for(i=0;i< newHeap->capacity;i++)
	newHeap->array[i] = A[i];

	newHeap->capacity = n;
	
//	for(i=0;i<newHeap->capacity;i++)
//	printf("%d : ",newHeap->array[i]);

	for(i=(n-1)/2;i>=0;i--){
	 percolateDown(newHeap,i);
	}

	for(i=0; i< newHeap->capacity;i++){
	printf("%d ",newHeap->array[i]);
	}

}

main(){

	struct Heap *h = createHeap(10,0);
	int A[10]={12,23,1,98,24,14,15,16,13,54};
	BuildHeap(h,A,10);

				

return 0;
}

