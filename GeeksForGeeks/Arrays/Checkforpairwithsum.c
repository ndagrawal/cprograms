#include<stdio.h>
#include<limits.h>
#include<stdlib.h>


void exchange (int *x,int *y){

int temp;

	temp = *x;
	*x=*y;
	*y=temp;

}
//then do the partition work .. ...
int partition(int A[],int left,int right){


	int down = left;
	int up=right;
	int a = A[left]; 
	int temp;

	while(down < up)
	{
		while(A[down] >=  a && down < up)
		down++;
		
		if(A[up] < a )
		up--;
		
		if(down<up)
		{
		  temp= A[up];
		  A[down]=A[up];
		  A[up]=temp;		
		}
	}
	
	temp = a;
	a=A[up];
	A[up]=temp;
		
return up;
}

//first do the quicksort....
void quicksort(int A[],int left,int right){
int pivot;

	if(left<right)
	{
		pivot = partition(A,left,right);
		quicksort(A,left,pivot-1);
		quicksort(A,pivot+1,right);
	}

}	
//then loop through all the elements of the array ... 
int hasArrayTwoCandidates(int A[],int size,int sum){

int left_index=0;
int right_index=size-1;

	quicksort(A,0,size-1);
	while(left_index < right_index){
		
	if(A[left_index] + A[right_index] == sum){
		return 1;
	}	
	else if(A[left_index] +A[right_index] >sum){
		left_index++;
 	    }
	
	else{
		right_index--;
	     }		
	}
return 0;
}

main(){
int A[]={10,12,3,9,8,21,8,10,11,5};
printf("Has Array two candidates = %d", hasArrayTwoCandidates(A,10,20));
return 0;
}
