#include<stdio.h>
#include<stdlib.h>

void productArray(int A[],int len) {

	int *left  = (int *) malloc(sizeof(int) * len);
	int *right = (int *) malloc(sizeof(int) * len);
	int *prod = (int *) malloc(sizeof(int) * len);
	int i;
	left[0]=1;
	right[len-1]=1;
	
	for(i=1;i<len;i++)
	left[i]= left[ i-1] * A[i-1];	

	for(i=len-2;i>=0;i--)
	right[i]=right[i+1] * A[i+1];

	for(i=0;i<len;i++)
	prod[i]=left[i] * right[i] ; 
	
	for(i=0;i<len;i++)
	printf(" %d",prod[i]);
}

main(){
int A[]={10,3,5,6,2};
int len=sizeof(A)/sizeof(int);
productArray(A,len);
return 0;
	}
